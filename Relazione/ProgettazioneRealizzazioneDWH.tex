\chapter{Progettazione e Realizzazione del Data Warehouse}

\label{chapter:ProgettazioneRealizzazioneDataWarehouse}

In questo capitolo si illustrerà la fase di definizione e realizzazione del \textit{data warehouse} del caso di studio, andando a presentare le fasi di progettazione concettuale e logica che producono rispettivamente lo schema DFM (acronimo di Dimensional Fact Model) e lo schema a stella/snowflake. Infine, verrà illustrato il processo di \texttt{ETL} dei dati presenti all'interno dell'ODS nel data warehouse, descrivendo l'aggiornamento dei dati relativi alle dimensioni ed al fatto.

\section{Progettazione Concettuale}

\label{section:DFM}

La prima fase che è stata analizzata ed affrontata è stata la fase di progettazione concettuale del data warehouse.

\begin{figure}[h!]
	\centering
	\includegraphics[width=1\textwidth]{Immagini/DFM}
	\caption{Schema concettuale (DFM) del data warehouse.}
	\label{fig:DFM}
\end{figure}

Nella figura \ref{fig:DFM} si presenta il DFM, il risultato della fase di progettazione concettuale del data warehouse. Questo schema permette di identificare il fatto di interesse, le sue misure e le sue varie dimensioni di analisi.

Innanzitutto, è possibile osservare con facilità che il fatto di interesse per le analisi, ovvero il concetto principale per il processo decisionale, è l'\textit{incidente}. La motivazione risiede nel fatto che il concetto principale di interesse di questo caso di studio è l'andamento ed il comportamento degli incidenti avvenuti nel comune di Roma nel periodo temporale 2010 - 2016. Osservando il fatto nella figura \ref{fig:DFM}, è possibile conoscere le misure che sono state pensate e definite per il fatto: sono 4 misure numeriche che rispettivamente, partendo dall'alto verso il basso, permettono di conoscere il numero di persone (sia pedoni, sia persone in auto) coinvolte in un incidente che sono rimaste ferite, morte, illese o che sono riportate in prognosi riservata.

Concentrandosi ora sulle dimensioni e gerarchie di dimensioni definite nel DFM, è possibile osservare che ne esistono differenti che descrivono e trattano informazioni diverse dell'incidente:
\begin{itemize}
	\item  è stato riportato un attributo descrittivo e degenere chiamato \textit{idProtocollo} relativo al fatto di interesse che rappresenta il codice identificativo dell'incidente presente nel dataset scaricato;
	\item la dimensione gerarchica chiamata \texttt{Data} permette di conoscere, a diversi livelli di aggregazione, la data in cui è avvenuto uno specifico incidente. In particolare, è stato pensato che fosse interessante, a livello di analisi, avere come informazioni il giorno in cui è avvenuto l'incidente (sia il numero nel mese sia il nome del giorno della settimana in cui è avvenuto), il mese, il bimestre, il trimestre, il quadrimestre, il semestre ed infine l'anno;
	\item la dimensione gerarchica chiamata \texttt{Orario} consente di sapere l'orario, visto come la composizione di ore, minuti e secondi, in cui è avvenuto l'incidente;
	\item la dimensione gerarchica chiamata \texttt{Meteo} permette di avere a disposizione informazioni sulle condizioni meteorologiche nella data in cui è avvenuto un incidente. In particolare, è possibile conoscere i fenomeni che si sono manifestati in un giorno, il vento massimo, le raffiche di vento, la temperatura massima, minima e media del giorno, i millimetri di precipitazioni e l'umidità; 
	\item le dimensioni che fanno riferimento al fatto chiamati \texttt{Illuminazione, Traffico, Visibilità, CondizioneAtmosferica, NaturaIncidente} e  \texttt{Gruppo} sono dimensioni degeneri che rispettivamente specificano l'illuminazione, il traffico, la visibilità, la condizione atmosferica della strada al momento dell'incidente, la natura dell'incidente ed il gruppo di polizia municipale che è intervenuto in soccorso nell'incidente;
	\newpage
	\item la dimensione chiamata \texttt{Strada} permette di conoscere la strada o le strade in cui è avvenuto un incidente. \uppercase{è} possibile notare che esiste un arco multiplo tra il fatto e questa dimensione: la motivazione è che un incidente specifico può coinvolgere più strade, mentre una strada può essere luogo di più incidenti;
	\item la dimensione gerarchica chiamata \texttt{Posizioni Geografiche} consente di avere informazioni sul luogo specifico dove è avvenuto l'incidente, come la latitudine, la longitudine, la presenza di segnaletica, la categoria ed il tipo di strada, la pavimentazione, il fondo stradale ed altre informazioni relative al luogo d'interesse;
	\item la dimensione gerarchica chiamata \texttt{Veicoli} che permette di conoscere informazioni sui veicoli coinvolti in un incidente, come ad esempio il tipo e lo stato del veicolo, la marca ed il modello. Osservando attentamente, è possibile notare che esiste un arco multiplo tra il fatto e questa dimensione: la motivazione è che un veicolo può essere coinvolto in più incidenti, mentre un incidente può coinvolgere più veicoli;
	\item la dimensione gerarchica chiamata \texttt{Persone} consente di avere informazioni sui pedoni e sulle persone in auto che sono state coinvolte in un incidente. Nella dimensione sono presenti gli attributi in comune tra i pedoni e le persone in auto. \uppercase{è} presente un attributo booleano chiamato \textit{inAuto} che permette di sapere se la persona che si sta considerando è una persona in auto (conducente o passeggero) oppure un pedone: nel primo caso ci sono altri quattro attributi che danno informazioni su una persona in auto. Inoltre, l'attributo booleano chiamato \textit{conducente} permette di distinguere se le persone in auto fossero conducenti o passeggeri al momento dell'incidente. L'arco che collega la dimensione \texttt{Veicoli} con la dimensione in considerazione associa il veicolo sul quale una persona in auto era presente in un incidente. Inoltre, esiste un arco multiplo tra il fatto e la dimensione considerata: la motivazione è che un incidente può coinvolgere più persone (sia pedoni sia persone in auto) e una persona può essere coinvolta in più incidenti.
\end{itemize} 

\section{Progettazione Logica}
Mentre la modellazione concettuale non è dipendente dal modello logico prescelto per l'implementazione, lo stesso non si può dire per i temi legati alla modellazione logica. Infatti, la struttura multi-dimensionale dei dati, tipica di un data warehouse, può essere rappresentata mediante due possibili distinti schemi logici: \textit{MOLAP} (acronimo di Multidimensional On-Line Analytical Processing) e \textit{ROLAP} (acronimo di Relational On-Line Analytical Processing). In questo progetto è stato scelto di realizzare il data warehouse con il modello ROLAP e per questo motivo il data warehouse del caso di studio verrà realizzato su database MySQL.

\subsection{Junk Dimension}
	Osservando lo schema concettuale illustrato nella figura \ref{fig:DFM}, si evince che esistono differenti dimensioni degeneri (\texttt{NaturaIncidente, CondizioneAtmosferica, Visibilità, Gruppo, Traffico, Illuminazione}), ovvero dimensioni la cui gerarchia contiene un solo attributo. Sono stati valutati i vantaggi e gli svantaggi delle soluzioni possibili per tradurre dimensioni degeneri, in particolar modo concentrandosi sulle due seguenti soluzioni: importare l'attributo nella \textit{fact table} oppure creare una \textit{junk dimension}. La prima soluzione è stata scartata perchè importare nel fatto sei attributi distinti non sembrava essere una soluzione ragionevole ed elegante dal punto di vista della modellazione. Per questo motivo, ci si è concentrati maggiormente sulla seconda soluzione proposta. \\
	Il primo passo è stato quello di analizzare i possibili valori assunti da ogni dimensione degenere e la relativa occupazione di memoria. Vengono riportati di seguito i risultati calcolati per ciascuna dimensione:
	\begin{itemize}
		\item \textit{illuminazione}: può assumere 4 valori distinti con una media di 11,25 caratteri ed il valore \textquotedblleft{}null\textquotedblright{}. Utilizzando 1 byte per memorizzare ogni carattere e 1 byte per il valore \textit{null} si ottiene: $ 11,25 \cdot 1 + 1 = 12,25 \approx 13$ byte necessari a memorizzare l'attributo;
		\item \textit{traffico}: può assumere 3 valori distinti con una media di 7 caratteri ed il valore \textquotedblleft{}null\textquotedblright{}. Utilizzando 1 byte per memorizzare ogni carattere e 1 byte per il valore \textit{null} si ottiene: $ 7 \cdot 1 + 1 = 8$ byte necessari a memorizzare l'attributo;
		\item \textit{visibilità}: può assumere 3 valori distinti con una media di 10 caratteri ed il valore \textquotedblleft{}null\textquotedblright{}. Utilizzando 1 byte per memorizzare ogni carattere e 1 byte per il valore \textit{null} si ottiene: $ 10 \cdot 1 + 1 = 11$ byte necessari a memorizzare l'attributo;
		\item \textit{gruppo}: può assumere 21 valori distinti interi. Si utilizzano 4 byte per memorizzare l'attributo;
		\item \textit{naturaIncidente}: può assumere 22 valori distinti con una media di 45 caratteri ed il valore \textquotedblleft{}null\textquotedblright{}. Utilizzando 1 byte per memorizzare ogni carattere e 1 byte per il valore \textit{null} si ottiene: $ 45 \cdot 1 + 1 = 46$ byte necessari a memorizzare l'attributo;
		\item \textit{condizioneAtmosferica}: può assumere 9 valori con una media di 9 caratteri ed il valore \textquotedblleft{}null\textquotedblright{}. Utilizzando 1 byte per memorizzare ogni carattere e 1 byte per il valore \textit{null} si ottiene: $ 9 \cdot 1 + 1 = 10$ byte necessari a memorizzare l'attributo.
	\end{itemize}

	A questo punto, si è valutata l'idea di creare un'unica \textit{junk dimension} che contenesse tutte le possibili combinazioni dei valori per tutti e sei gli attributi. \\
	La memorizzazione di un singolo record richiede $ 13+8+11+4+46+10 = 92$ byte
	e il numero di record della tabella è determinato dalle combinazioni di tutti i possibili valori. L'occupazione in memoria di tale tabella risulta pari a
	\begin{equation}
		92 \cdot 5 \cdot 4 \cdot 4 \cdot 21 \cdot 22 \cdot 10 = 34003200 \; B = 32,42 \; MB
	\end{equation}
	
	Questa soluzione è stata scartata sia perchè il numero di record della tabella risulta elevato (369600 record), sia perchè, a fronte di una variazione del numero di attributi, le dimensioni della tabella tendono a crescere repentinamente. Se, per esempio, le condizioni del traffico potessero assumere 5 valori distinti invece che 4, la tabella occuperebbe
	\begin{equation}
		92 \cdot 5 \cdot 5 \cdot 4 \cdot 21 \cdot 22 \cdot 10 = 42504000 \; B = 40,53 \; MB
	\end{equation}
	Si avrebbe, quindi, un aumento di circa 8 MB di memoria al variare di un solo attributo. \\
	La soluzione adottata è stata quella di separare gli attributi, creando due \textit{junk dimension}: la prima chiamata \texttt{CondizioniIncidenti}, contenente tutte le possibili combinazioni dei valori degli attributi \textit{illuminazione, visibilità, traffico, condizioneAtmosferica}, ed una seconda chiamata \texttt{InformazioniIncidenti}, contenente tutte le possibili combinazioni dei valori degli attributi \textit{gruppo, naturaIncidente}.\\
	La cardinalità della prima \textit{junk dimension} è $5 \cdot 4 \cdot 4 \cdot 10 = 800$ record, che porta ad una occupazione di memoria di 
	\begin{equation}
		(13+11+10+8) \cdot 5 \cdot 4 \cdot 4 \cdot 10 = 42 \cdot 800 = 33600 \; B = 32,81 \; KB
	\end{equation}
	
	La cardinalità della seconda \textit{junk dimension} è $21 \cdot 22 = 462$ record, che porta ad una occupazione di memoria di 
	\begin{equation}
		(46+4) \cdot 21 \cdot 22 = 50 \cdot 642 = 23100 \; B = 22,55 \; KB
	\end{equation}
	In entrambe le \textit{junk dimension}, infine, sono state inserite delle chiavi primarie surrogate (numeri interi auto-incrementali).

 \subsection{Dimensione Persone}
	Osservando attentamente lo schema concettuale in figura \ref{fig:DFM}, si evince l'esistenza di una gerarchia di dimensioni chiamata \texttt{Persone}. Per trasformare questa gerarchia in una o più relazioni logiche, è stata condotta un'analisi delle possibili soluzioni attuabili, tenendo in considerazione dei vantaggi e degli svantaggi di ciascuna. La prima soluzione prevede l'accorpamento di tutti gli attributi di questa gerarchia all'interno di un'unica relazione, mentre la seconda soluzione prevede la suddivisione in due relazioni distinte i record facenti riferimento ai pedoni ed i record facenti riferimento alle persone in auto. Il principale svantaggio della prima soluzione è che i record che fanno riferimento ai pedoni avranno per i 5 attributi \textit{airbag, cinturaCascoUtilizzato, progressivo, conducente} e \textit{Veicoli\_idVeicolo} valori \texttt{null} perché questi attributi descrivono delle caratteristiche solo per le persone in auto. Il vantaggio di questa soluzione è che in una sola relazione è possibile disporre di tutte le informazioni sulle persone coinvolte in un incidente, evitando di dover importare nel fatto una chiave esterna aggiuntiva. Il vantaggio della seconda soluzione, invece, è la suddivisione a livello concettuale delle informazioni in due relazioni distinte, a seconda del tipo di persona che si sta considerando (pedone o persona in auto).
	\\
	Il numero di persone in auto è di 112625, mentre quello dei pedoni è 16168.
	Se si sceglie la prima soluzione e ipotizzando che occorra 1 byte per memorizzare il valore \texttt{null}, si avrebbe che gli attributi per i pedoni a \texttt{null} occuperebbero
	\begin{equation}
		16168 \cdot 5 \;attributi \cdot 1 = 80840 \; B = 0,07 \; MB
	\end{equation}
	
	Optando invece per la seconda soluzione, occorrerebbero 4 byte per memorizzare la chiave della dimensione pedoni all'interno del fatto. Il numero degli incidenti è 228505, quindi aggiungere la chiave comporterebbe un aumento della memoria utilizzata di 
	\begin{equation}
		228505 \cdot 4 = 914020 \; B = 0,87 \; MB
	\end{equation}
	
	La scelta è ricaduta sulla prima soluzione sia per la minor occupazione di memoria, sia perchè, in un'ottica futura al crescere del numero di incidenti, la seconda soluzione porterebbe ad un rapido aumento di memoria rischiesta.

\subsection{Helper Table}
\label{sottosezione:helperTable}

Nello schema concettuale mostrato in figura \ref{fig:DFM}, si può osservare l'esistenza di archi multipli tra il fatto \texttt{Incidente} e le dimensioni \texttt{Veicoli, PersoneCoinvolte} e \texttt{Strade}. Le due principali soluzioni analizzate sono: utilizzare una \textit{bridge table} oppure una \textit{helper table}. Il principale svantaggio di entrambe è che non è più possibile mantenere uno schema a stella, ma si passerebbe ad uno \textit{schema snowflake} (o anche detto, a fiocco di neve), uno schema con distanza dal fatto ad una dimensione maggiore di 1; questo perché bisognerebbe introdurre una relazione per modellare l'arco multiplo. Il vantaggio della prima soluzione è che la \textit{bridge table} ha come chiave primaria la composizione delle due chiavi primarie delle due relazioni, come se fosse una relazione che implementa una relazione molti-a-molti nel modello logico. Il vantaggio della seconda soluzione è che non incrementa la cardinalità della \textit{fact table}, ma aggiunge solo una chiave esterna. La soluzione adottata è l'\textit{alternativa K} di Ralph Kimball che prevede la definizione di una \textit{helper table} per realizzare la traduzione dell'arco multiplo dal modello concettuale al modello logico . Per questo motivo, sono stata definite tre relazioni chiamate rispettivamente \texttt{Helper\_Incidenti\_Veicoli}, \texttt{Helper\_Incidenti\_Persone} e \texttt{Helper\_Incidenti\_Strade}.
\\ Forniamo ora un esempio per comprendere al meglio la soluzione attuata. Concentrando l'attenzione sugli archi multipli tra il fatto \texttt{Incidente} e \texttt{Strade}, questo costrutto viene trasformato nel modello logico-relazionale creando una \textit{helper table} chiamata \texttt{Helper\_Incidenti\_Strade} che ha come chiave primaria un identificativo intero auto-incrementale e che contiene al suo interno, come chiave esterna, la chiave primaria della relazione \texttt{Strade}, in modo tale che sia possibile effettuare un'operazione di \textit{join} tra queste ultime due relazioni. Se un incidente avviene su più strade, nel fatto viene riportata la chiave corrispondente al gruppo corrispondente alle strade in questione. Nello stesso identico modo, sono state modellate le helper table tra il fatto e le relazioni \texttt{Veicoli} e \texttt{PersoneCoinvolte}.
	
\subsection{Scenari Temporali}

Il modello multi-dimensionale assume che gli eventi che istanziano un fatto siano dinamici, mentre i valori degli attributi che popolano le gerarchie siano statici. Questa visione non è realistica poiché anche i valori presenti nelle gerarchie variano nel tempo dando vita alle \textit{gerarchie dinamiche} (anche dette Slowly Changing Dimensions). L'adozione di gerarchie dinamiche implica un sovraccosto in termini di spazio e può comportare una forte riduzione delle prestazioni.

Dopo un'analisi delle tre possibili soluzioni implementabili, dette rispettivamente \textit{Oggi per Ieri}, \textit{Oggi o Ieri} e \textit{Oggi e Ieri}, si è deciso di realizzare la seconda soluzione. Scegliendo questa soluzione, i dati vengono interpretati in base alla configurazione valida al momento in cui sono stati registrati e consentono di registrare la verità storica. Il motivo per cui si è scelto di adottare questa soluzione risiede nel fatto che le informazioni nelle dimensioni di analisi non prevedono aggiornamenti nel tempo di record già presenti, ma solo un inserimento incrementale di nuovi dati. Gli eventi memorizzati nella \textit{fact table} vengono associati ai dati dimensionali che erano validi quando si è verificato l'evento. Questa soluzione è realizzabile sullo schema snowflake: ogni modifica a una gerarchia comporta l'inserimento di un nuovo record che codifichi le nuove caratteristiche nella \textit{dimension table} corrispondente.
	
\subsection{Schema Snowflake}
A partire dallo schema concettuale del data warehouse presentato e discusso nella sezione \ref{section:DFM}, la fase successiva è quella di trasformare lo schema concettuale nello schema logico-relazionale corrispondente. Per effettuare ciò, è necessario attenersi alle regole di trasformazione di uno schema DFM in uno schema snowflake, tenendo in considerazione il volume dei dati, il carico di lavoro stimato del data warehouse ed i vincoli di sistema.

L'idea principale della trasformazione da schema concettuale a schema logico è produrre uno \textit{schema a stella}. In generale, questo schema è composto da:
\begin{itemize}
	\item un insieme di relazioni DT\ped{1}, DT\ped{2}, \ldots, DT\ped{n} chiamate \textit{dimension table} ciascuna corrispondente ad una dimensione del DFM. Ogni DT\ped{i} è caratterizzata da una chiave primaria (tipicamente è un identificativo intero surrogato auto-incrementale) e da un insieme di attributi che descrivono le dimensioni di analisi a diversi livelli di aggregazione;
	\item una relazione FT chiamata \textit{fact table} che importa le chiavi di tutte le dimension table ed inoltre contiene un attributo per ogni misura del DFM.
\end{itemize}

La regola di base che è stata seguita per la definizione dello schema a stella è la seguente: \textquotedblleft \textit{creare una fact table contenente tutte le misure e gli attributi descrittivi direttamente collegati con il fatto e, per ogni gerarchia, creare una dimension table che ne contiene tutti gli attributi}\textquotedblright.

Come già anticipato nella sottosezione \ref{sottosezione:helperTable}, la presenza di archi multipli e la relativa modellazione tramite \textit{helper table}, ha \textquotedblleft{}obbligato\textquotedblright{} a realizzare uno \textit{snowflake schema} come modello logico-relazionale del data warehouse (al posto dello schema a stella), riportato in figura \ref{fig:SchemaLogicoDWH} con la notazione offerta da \textit{MySQL Workbench}. \uppercase{è} possibile conoscere il nome di ciascuna relazione definita, il nome ed il tipo degli attributi e le associazioni che esistono tra le relazioni. Osservando attentamente la figura, è possibile comprendere che tutte le relazioni che sono state definite hanno un attributo chiamato \textit{updateTime} che consente di avere informazioni sulla data di aggiornamento di ciascun record nella relazione considerata.

\begin{figure}[h!]
	\centering
	\includegraphics[width=1\textwidth]{Immagini/SchemaLogicoDWH}
	\caption{Schema (snowflake) logico-relazionale del data warehouse.}
	\label{fig:SchemaLogicoDWH}
\end{figure}

\section{Processo di ETL dei Dati dall'ODS al Data Warehouse}

Il processo di ETL ha lo scopo di estrarre i dati dall'ODS, applicare differenti trasformazioni che consentono di rendere i dati dell'ODS \textquotedblleft omogenei\textquotedblright{} e normalizzati con la definizione delle relazioni a livello logico-relazionale nel data warehouse e caricare i dati trasformati dall'ODS all'interno del data warehouse. Entrando nel dettaglio, l'estrazione dei dati dall'ODS è \textit{incrementale}, rilevando solo gli aggiornamenti rispetto all'ultima estrazione (inserimenti, cancellazioni ed aggiornamenti) ed è di tipo \textit{ritardata}, nel senso che la modifica viene rilevata solo a posteriori. Questa estrazione è basata sull'utilizzo di \textit{marche temporali}. Un'osservazione molto importante ai fini della correttezza del presente processo di ETL è che le dimensioni da aggiornare devono seguire un ordine ben definito: prima si aggiornano le dimensioni più esterne e successivamente si aggiornano le dimensioni più interne (le \textit{helper table}). In figura \ref{fig:ETLDWH} viene mostrato il \textit{workflow} del processo ETL implementato per il caricamento dei dati dall'ODS al data warehouse.

\begin{figure}[h!]
	\centering
	\includegraphics[width=1\linewidth]{Immagini/ETL_DWH}
	\caption{Processo di ETL dei dati dall'ODS al data warehouse.}
		\label{fig:ETLDWH}
\end{figure}

Sono state implementate due classi \texttt{java} chiamate rispettivamente \textit{EtlStarter} e \textit{createFact} (presenti nella cartella \texttt{/Sorgenti JAVA}) che consentono di eseguire in pipeline tutto il processo di ETL dei dati riguardanti rispettivamente l'aggiornamento delle dimensioni e del fatto.


\subsection{Aggiornamento delle Dimensioni} \label{subsection:ETLDimensioniODSDWH}

Il processo di aggiornamento delle dimensioni è stato sviluppato seguendo i passi del seguente algoritmo:

\begin{enumerate}
	\item controllo all'interno del data warehouse se la dimensione non contiene record. Se è vuota, vengono identificati i valori univoci di tutti i record presenti nell'ODS e vengono caricati nella dimensione di destinazione tramite l'esecuzione di uno \texttt{script SQL} sfruttando il comando \texttt{LOAD DATA LOCAL INFILE};
	\item nel caso in cui, invece, la dimensione non sia vuota, si procede nel seguente modo:
		\begin{enumerate}
			\item vengono prelevati tutti i record presenti nell'ODS la cui data di inserimento o di aggiornamento risulta maggiore del valore più aggiornato dell'attributo \textit{updateTime} nella dimensione corrispondente nel data warehouse;
			\item si controlla se nell'insieme dei record appena estratti vi siano valori non presenti nella dimensione e si procede al salvataggio di tali nuove informazioni in un file CSV;
			\item si esegue uno \texttt{script SQL} che, sfruttando il comando \texttt{LOAD DATA LOCAL INFILE}, carica i record presenti nel file creato nello step precedente all'interno del data warehouse, nella relazione corretta.
		\end{enumerate}
	\end{enumerate}

Il salvataggio intermedio in un file CSV non è un'operazione necessaria al processo di alimentazione del data warehouse, ma ha consentito una miglior gestione del processo ed un migliore controllo dei dati durante il flusso.

Le classi \texttt{java} \textit{UpdateVeicoli, UpdateStrade, UpdatePedoniPersoneInAuto, UpdateLuoghi, UpdateDataOraMeteo, JunkDimension} permettono rispettivamente l'aggiornamento delle dimensioni \textit{Veicoli, Strade, PersoneCoinvolte, Luoghi, Data, Orario, Meteo, CondizioniIncidenti e InformazioniIncidenti} presenti nel data warehouse con dati provenienti dall'ODS. Le tre classi \texttt{java} chiamate \textit{HelperTableVeicoli, HelperTableStrade} e \textit{HelperTablePersoneCoinvolte} permettono l'aggiornamento rispettivamente delle \textit{helper table} relative ai veicoli, alle strade ed alle persone coinvolte. In particolare, aggiornare le \textit{helper table} significa controllare se vi sia o meno lo stesso gruppo di record associato al medesimo incidente.

Al fine di garantire un esito corretto, il processo di aggiornamento deve seguire il seguente ordine di esecuzione:
\begin{enumerate}
	\item aggiornamento delle dimensioni \texttt{Data} e \texttt{Orario};
	\item aggiornamento della dimensione \texttt{Meteo};
	\item aggiornamento della dimensione \texttt{Strade};
	\item aggiornamento della \texttt{HelperTableStrade};
	\item aggiornamento della dimensione \texttt{Veicoli};
	\item aggiornamento della \texttt{HelperTableVeicoli};
	\item aggiornamento della dimensione \texttt{PosizioniGeografiche};
	\item aggiornamento delle junk dimension \texttt{CondizioniIncidenti} e \sloppypar{\texttt{InformanzioniIncidenti}};
	\item aggiornamento della dimensione \texttt{Persone};
	\item aggiornamento della \texttt{HelperTablePersoneCoinvolte}.
\end{enumerate}  

Le dimensioni \texttt{Data} e \texttt{Orario} sono dimensioni standard e sono quindi state create manualmente e caricate nel data warehouse.
 
\subsection{Creazione del Fatto}

Una volta aggiornati i dati presenti nelle relazioni dello schema logico-relazionale del data warehouse che fanno riferimento alle dimensioni definite nello schema concettuale tramite le operazioni illustrate nella sezione \ref{subsection:ETLDimensioniODSDWH}, è possibile effettuare il processo di ETL destinato alla creazione del fatto \texttt{Incidente}. Anche in questo caso, la creazione dei nuovi fatti è \textit{incrementale}, andando ad aggiungere nel data warehouse solo i nuovi dati mantenendo la loro storicità.

Per effettuare questo processo, è stata implementata una classe \texttt{java} che realizza le seguenti operazioni:

\begin{enumerate}
	\item ricerca dei valori dei record prelevati nelle relative dimensioni e prelevamento delle chiavi primarie dei valori corrispondenti;
	\item creazione e scrittura su file in formato CSV di tutti i record che devono essere caricati nella relazione \texttt{Incidenti} del data warehouse che si riferisce al fatto nel DFM;
	\item esecuzione di uno \texttt{script SQL} che, sfruttando il comando \texttt{LOAD DATA LOCAL INFILE}, carica i record presenti nel file creato nello step precedente all'interno del data warehouse nella relazione \texttt{Incidenti}. Per maggiori dettagli, si faccia riferimento allo script\footnote{Script\_Caricamento\_Fatto\_DWH.sql} presente nella cartella \texttt{/ScriptSQL}.
\end{enumerate}

Il salvataggio intermedio in un file CSV non è un'operazione necessaria al processo di alimentazione del data warehouse, ma ha consentito una miglior gestione del processo ed un migliore controllo dei dati durante il flusso.