\chapter{Livello Riconciliato dei Dati - ODS}

\label{chapter:LivelloRiconciliatoDatiODS}

In questo capitolo, si illustrerà il processo di definizione dell'\textit{ODS} (acronimo di Operational Data Store), una base di dati progettata per integrare dati dalle due fonti per operazioni addizionali sui dati. In particolare, si presenterà la prima fase di definizione dello schema concettuale e la successiva fase di definizione dello schema logico-relazionale dell'ODS. Infine, verrà illustrato il processo di ETL (acronimo di Extract, Transform, Load) che consente di caricare i dati estratti dalle due sorgenti dati operazionali all'interno dell'ODS, dopo l'applicazione di trasformazioni nei dati.

\section{Schema Concettuale}

A partire dagli schemi concettuali delle due sorgenti dati presentati nel capitolo \ref{chapter:AnalisiSorgentiDati}, è possibile produrre lo schema concettuale riconciliato dell'ODS, illustrato nella figura \ref{fig:SchemaConcettualeRiconciliatoODS}. L'integrazione dei due schemi concettuali in un unico schema è stato un processo semplice e veloce siccome le due sorgenti dati trattano di temi differenti e hanno schemi che non si sovrappongono o intersecano in nessun punto. Per effettuare quest'integrazione, è stato necessario collegare l'entità \textit{Meteo} presente nella sorgente dati \texttt{Meteo} con l'entità \textit{Incidente} presente nella sorgente dati \texttt{Incidenti Stradali} definendo un'associazione \textit{uno-a-molti} tra le due entità: uno specifico incidente ha associato una specifica data di rilevazione del meteo, mentre un meteo identificato tramite la data di rilevazione può avere associato più incidenti avvenuti in una specifica data. Questo collegamento è stato realizzato grazie alla presenza nell'entità \texttt{Meteo} di un attributo chiamato \textit{data} che si riferisce alla data di rilevazione della condizione meteorologica nel comune di Roma ed alla presenza nell'entità \texttt{Incidente} di un attributo chiamato \textit{dataIncidente} che si riferisce alla data in cui è avvenuto uno specifico incidente.

\begin{figure}[h!]
	\centering
	\includegraphics[width=\textwidth]{Immagini/SchemaConcettualeRiconciliatoODS}
	\caption{Schema concettuale dell'ODS.}
	\label{fig:SchemaConcettualeRiconciliatoODS}
\end{figure}

\section{Schema Logico-Relazionale}

Lo schema logico-relazionale dell'ODS, riportato nella figura \ref{fig:SchemaLogicoODS} con la notazione offerta da \textit{MySQL Workbench}, è possibile ottenerlo in modo semplice e veloce a partire dallo schema concettuale riconciliato dell'ODS. \uppercase{è} possibile osservare che lo schema logico-relazionale dell'ODS così ottenuto coincide semplicemente con l'unione degli schemi logici-relazionali delle due sorgenti dati.

Le relazioni definite nell'ODS sono le seguenti:

\texttt{Veicoli} (\textbf{\underline{idVeicolo}}, marca, modello, statoVeicolo, tipoVeicolo, progressivo, \textit{idProtocollo}, annoRiferimento, insertTime, updateTime)

\texttt{Pedoni} (\textbf{\underline{idPedone}}, annoNascita, sesso, tipoPersona, tipoLesione, deceduto, decedutoDopo, \textit{idProtocollo}, annoRiferimento, insertTime, updateTime)

\texttt{PersoneInAuto} (\textbf{\underline{idPersonaInAuto}}, progressivo, tipoPersona, annoNascita, sesso, tipoLesione, deceduto, cinturaCascoUtilizzato, airbag,  decedutoDopo, \textit{idProtocollo}, \textit{idVeicolo}, annoRiferimento, insertTime, updateTime)

\texttt{Incidenti} (\textbf{\underline{idIncidente}}, gruppo, dataIncidente, oraIncidente, condizioneAtmosferica, naturaIncidente, numeroRiservata, numeroIllesi, numeroMorti, numeroFeriti, confermato, illuminazione, visibilità, traffico, \textit{idLuogo}, annoRiferimento, insertTime, updateTime)

\texttt{Luoghi} (\textbf{\underline{idLuogo}}, latitudine, longitudine, chilometrica, daSpecificare, particolaritàStrade, localizzazione, categoriaStrada, tipoStrada, fondoStradale, pavimentazione, segnaletica, annoRiferimento, insertTime, updateTime)

\texttt{Strade} (\textbf{\underline{idStrada}}, strada, annoRiferimento, insertTime, updateTime)

\texttt{Luoghi\_Strade} (\textbf{\underline{\textit{idLuogo}}}, \textbf{\underline{\textit{idStrada}}}, annoRiferimento, insertTime, updateTime)

\texttt{Meteo} (\textbf{\underline{data}}, temperaturaMinima, temperaturaMassima, temperaturaMedia, precipitazioni, raffica, umidità, fenomeni, ventoMassimo, annoRiferimento, insertTime, updateTime)

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.9\textwidth]{Immagini/SchemaLogicoODS}
	\caption{Schema logico-relazionale dell'ODS.}
	\label{fig:SchemaLogicoODS}
\end{figure}

\newpage

\uppercase{è} stato scritto uno \texttt{script SQL} che ha permesso di implementare l'ODS a livello logico-relazionale nel database MySQL. Per maggiori dettagli, si faccia riferimento allo script\footnote{Script\_Creazione\_ODS.sql} presente nella cartella \texttt{/Script SQL}.

Osservando le relazioni che sono state definite nello schema logico-relazionale dell'ODS, è possibile notare che esiste un attributo presente in tutte le relazioni chiamato \textit{annoRiferimento}, utile per conoscere l'anno di riferimento (2010 o 2011 o 2012 o 2013 o 2014 o 2015 oppure 2016) di un record della relazione.

I vincoli di integrità referenziale definiti sulle relazioni sono i seguenti:
\begin{itemize}
	\item Veicoli.idProtocollo $\rightarrow$ Incidenti.idIncidente
	\item Pedoni.idProtocollo $\rightarrow$ Incidenti.idIncidente
	\item PersoneInAuto.idProtocollo $\rightarrow$ Incidenti.idIncidente
	\item PersoneInAuto.idVeicolo $\rightarrow$ Veicoli.idVeicolo
	\item Incidenti.idIncidente $\rightarrow$ Luoghi.idLuogo
	\item Posizionamento.idLuogo $\rightarrow$ Luoghi.idLuogo
	\item Posizionamento.idStrada $\rightarrow$ Strade.idStrada
	\item Meteo.data $\rightarrow$ Incidenti.dataIncidente
\end{itemize}

\section{Processo di ETL dei Dati dalle Sorgenti Operazionali all'ODS}

Il processo di \texttt{ETL} (acronimo di Extract, Transform. Load) ha lo scopo di estrarre i dati dalle due sorgenti operazioni, i cui database contenenti i dati si chiamano rispettivamente \texttt{Incidenti\_DB} e \texttt{Meteo\_DB}, applicare differenti trasformazioni che consentono di ripulire e migliorare la qualità dei dati e caricare i dati trasformati nell'ODS. Per effettuare questo processo, viene definita una \texttt{Staging Area}, un'area di memorizzazione di dati temporanea intermedia che viene rimossa una volta concluso il processo di ETL. L'estrazione dei dati dalle sorgenti operazionali è \textit{incrementale}, rilevando solo gli aggiornamenti rispetto all'ultima estrazione (inserimenti, cancellazioni ed aggiornamenti) ed è di tipo \textit{ritardata}, nel senso che la modifica viene rilevata solo a posteriori. Questa estrazione è basata sull'utilizzo di \textit{marche temporali}.

\newpage

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.75\textwidth]{Immagini/ETL_ODS}
	\caption{Fasi del processo di ETL dei dati dalle sorgenti operazionali all'ODS.}
	\label{fig:FasiProcessoETLSorgentiOperazionaliODS}
\end{figure}

Nella figura \ref{fig:FasiProcessoETLSorgentiOperazionaliODS} è possibile osservare il \textit{workflow} del processo di estrazione, trasformazione e caricamento dei dati dalle due sorgenti nell'ODS. Per effettuare questo processo, è stato definito uno \texttt{script SQL} che realizza le seguenti operazioni:
\begin{enumerate}
	\item viene creata una prima relazione temporanea (chiamata ad esempio \textit{relazione\_stagingArea}) in cui verranno memorizzate temporaneamente le informazioni relative alla relazione in considerazione della sorgente;
	\item vengono prelevati tutti i record presenti nella relazione in considerazione tali per cui il valore dell'attributo \textit{updateTime} è maggiore della data più aggiornata di \textit{updateTime} nella relazione considerata nell'ODS;
	\item i record reperiti nello step precedente vengono caricati nella relazione creata nello step 1;
	\item viene creata una seconda relazione temporanea (chiamata ad esempio \textit{relazione\_stagingArea\_trasformati}) che consente di memorizzare le informazioni relative alla relazione in considerazione presente nella staging area dello step 1;
	\item vengono applicate le trasformazioni opportune sui valori assunti a seconda dell'attributo considerato;
	\item i record dello step 2 vengono caricati nella relazione creata nello step 4;
	\item i record presenti nella \textit{staging area trasformati} vengono inseriti nella corretta relazione dell'ODS. I valori degli attributi \textit{insertTime} e \textit{updateTime} vengono aggiornati con il valore della data dell'operazione.
\end{enumerate}

Le trasformazioni che sono state applicate sulle relazioni sono le seguenti:
\begin{itemize}
	\item se il valore di un attributo è vuoto, viene inserito il valore \texttt{null} di MySQL;
	\item il tipo dell'attributo \textit{deceduto} delle relazioni \texttt{PersoneInAuto} e \texttt{Pedoni} viene trasformato da stringa di caratteri a booleano;
	\item se il valore degli attributi \textit{precipitazioni}, \textit{raffica}, \textit{ventoMassimo} e \textit{umidità} della relazione \texttt{Meteo} è uguale al simbolo \textquotedblleft-\textquotedblright, allora viene sostituito con il valore \texttt{null} di MySQL;
	\item siccome il valore dell'attributo \textit{annoNascita} delle relazioni \texttt{PersoneInAuto} e \texttt{Pedoni} può assumere valori non possibili (per problemi di qualità dei dati), come ad esempio 0, in questo caso il valore dell'attributo viene impostato a \texttt{null} di MySQL;
	\item se il valore dell'attributo \textit{progressivo} della relazione \texttt{PersoneInAuto} è uguale a 0, allora viene impostato il valore dell'attributo \textit{progressivo} della relazione \texttt{Veicoli}.
\end{itemize}

Per maggiori dettagli, si faccia riferimento allo script\footnote{Script\_CreazionePopolamento\_StagingArea\_ODS.sql} presente nella cartella \texttt{/ScriptSQL}.