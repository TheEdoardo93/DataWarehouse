 \chapter{Analisi delle Sorgenti Dati}
 
 \label{chapter:AnalisiSorgentiDati}

In questo capitolo verranno presentate ed analizzate le due sorgenti dati utilizzate come punto di partenza del progetto, descrivendo la metodologia di reperimento dei dati, quali informazioni sono disponibili, gli schemi logici-relazionali, e tramite un processo di reverse engineering, gli schemi concettuali. Inoltre, verrà presentata una sezione in cui verranno illustrate le interviste realizzate tramite un'interazione con gli utenti finali del sistema, un glossario dei requisiti ed una stima del carico di lavoro preliminare che consente di definire quali analisi si vorranno eseguire con i dati disponibili. Infine, si presenterà una sezione che analizza la qualità dei dati disponibili delle due sorgenti dati, evidenziando problemi e limiti presenti.

\section{Sorgente Dati: Incidenti Stradali}

\subsection{Reperimento dei Dati}

Il dataset relativo agli incidenti avvenuti nel comune di Roma è stato reperito dal portale \textit{open data} del sito del comune di Roma Capitale, in particolare nella sezione chiamata \textquotedblleft Incidenti Stradali\footnote{\url{http://dati.comune.roma.it/cms/it/incidenti_stradali.page}}\textquotedblright. La fonte del dataset è la Polizia Locale Roma Capitale - U.O. Organizzazione Controllo di Gestione e Sistemi Informativi - Sezione Sistemi Informativi ed hanno una licenza \textit{Creative Commons BY 4.0}. I dati sono disponibili e pronti per essere scaricati nei formati \texttt{.json} oppure \texttt{.xml}.

\subsection{Analisi del Dataset}

\label{subsection:AnalisiDatasetIncidenti}

Gli incidenti avvenuti nel comune di Roma negli anni 2010 - 2016 sono descritti dai seguenti quattro dataset, che nel loro insieme offrono una panoramica completa dei sinistri registrati:
\begin{enumerate}
	\item il primo dataset, chiamato d'ora in poi \textit{Pedoni}, descrive la relazione che sussiste tra gli incidenti avvenuti a Roma ed i pedoni che sono stati coinvolti in questi incidenti;
	\item il secondo dataset, chiamato d'ora in poi \textit{Persone in Auto}, descrive la relazione che sussiste tra gli incidenti avvenuti a Roma e le persone in auto che sono state coinvolte in questi incidenti;
	\item il terzo dataset, chiamato d'ora in poi \textit{Veicoli}, descrive la relazione che sussiste tra gli incidenti avvenuti a Roma ed i veicoli che sono stati coinvolti in questi incidenti;
	\item il quarto dataset, chiamato d'ora in poi \textit{Incidenti}, descrive diverse informazioni a riguardo degli incidenti avvenuti nel comune di Roma in cui è intervenuta la Polizia Locale.
\end{enumerate}

In particolare, dai quattro dataset appena elencati è possibile reperire ed ottenere le seguenti informazioni:
\begin{enumerate}
	\item sui \textit{pedoni} è possibile conoscere il codice identificativo dell'incidente nel quale è stato coinvolto il pedone, l'anno di nascita, il sesso, il tipo di lesione procurata nel coinvolgimento dell'incidente, se il pedone è deceduto oppure non è deceduto dopo essere stato coinvolto nell'incidente e, nel caso in cui non sia deceduto immediatamente durante l'incidente, se è deceduto successivamente;
	\item sulle \textit{persone in auto} è possibile conoscere il codice identificativo dell'incidente nella quale è stata coinvolta una persona, il codice identificativo del veicolo nella quale una persona si trovava al momento dell'incidente, un codice progressivo che nel caso di più veicoli coinvolti in un determinato incidente indica a quale dei veicoli la persona appartiene, il tipo di persona (se era un conducente oppure un passeggero nell'incidente nel quale è stato coinvolto), l'anno di nascita, il sesso, il tipo di lesione procurata nel coinvolgimento dell'incidente, se la persona in auto è deceduta oppure non è deceduta dopo essere stata coinvolta nell'incidente e, nel caso in cui non sia deceduta immediatamente durante l'incidente, se è deceduta successivamente, se la persona indossava la cintura di sicurezza (nel caso di automobile) o il casco (nel caso di ciclomotore o motociclo) e se è intervenuto o meno l'airbag al momento dell'incidente;
	\item sui \textit{veicoli} è possibile conoscere il codice identificativo dell'incidente nel quale è stato coinvolto un veicolo, un codice progressivo che enumera i veicoli coinvolti nello stesso incidente, un codice identificativo del veicolo, il tipo di veicolo (automobile, motociclo, \ldots), lo stato del veicolo (se era in marcia, in sosta, in fermata, \ldots), la marca ed il modello del veicolo;
	\item sugli \textit{incidenti} è possibile conoscere il codice identificativo dell'incidente, il gruppo di polizia municipale intervenuto, la data e l'ora dell'incidente, la strada oppure le strade nella quale è avvenuto un incidente, la natura dell'incidente (scontro frontale, tamponamento, veicolo in marcia contro ostacolo fisso, \ldots), eventuale particolarità delle strade (incrocio, curva, rettilineo, \ldots), il tipo di strada (una carreggiata a senso unico, due carreggiata a doppio senso, \ldots), il fondo stradale (bagnato, asciutto, ghiacciato, \ldots), la pavimentazione delle strade (asfaltata, porfido, \ldots), la segnaletica presente sulle strade (verticale, orizzontale, entrambe), la condizione atmosferica al momento dell'incidente (sole, nuvole, neve, pioggia, \ldots), l'entità del traffico (scarso, normale, intenso), la visibilità (insufficiente, sufficiente, buona, ottima) e l'illuminazione (insufficiente, sufficiente, ore diurne), il numero di persone che sono morte, illese, ferite e in prognosi riservata durante l'incidente, la latitudine e la longitudine (in gradi decimali) che permettono di identificare la posizione geografica dove è avvenuto l'incidente e se l'incidente è stato confermato oppure non è stato confermato dalla polizia locale. Gli attributi descrivono l'esatto punto dell'incidente, quindi ad esempio se la segnaletica è registrata come \textquotedblleft orizzontale\textquotedblright{}, significa che in quella specifica posizione la segnaletica è \textquotedblleft orizzontale\textquotedblright{}, ma la strada su cui è avvenuto l'incidente può avere anche altri tipi di segnaletica.
\end{enumerate}

\begin{comment}
\subsection{Schema Logico-Relazionali delle Sorgenti Dati}

\label{section:SchemiLogiciRelazionaliSorgentiDati}

In questa sezione, si illustrano gli schemi logici-relazionali delle due sorgenti dati oggetto del caso di studio. In particolare, verranno elencate le chiavi primarie, le chiave esterne e gli attributi presenti in ciascuna relazione individuata oppure ipotizzata e verranno discusse le motivazioni che hanno portato a scelte di modellazione logico-relazionale dei dataset.

\textit{Nota.} Negli schemi logici-relazionali riportati di seguito, si è assunta la seguente notazione:
\begin{itemize}
	\item si è deciso di sottolineare e mettere in grassetto la chiave primaria di ciascuna relazione;
	\item si è deciso di mettere in corsivo la chiave esterna o le chiavi esterne presenti in una relazione che fanno riferimento ad altre relazioni.
\end{itemize}
\end{comment}

\subsection{Schema Logico-Relazionale}

Gli schemi logici-relazionali dei quattro dataset descritti nella sotto-sezione \ref{subsection:AnalisiDatasetIncidenti} non sono stati forniti insieme ai dati e non è stato possibile ottenerli da altre fonti. Si è ipotizzata quindi una soluzione ragionevole e concreta in termini di correttezza del formalismo definito dagli schemi logici-relazionali.

Di seguito verranno descritti i quattro dataset relativi ai pedoni, alle persone in auto, ai veicoli ed agli incidenti, mentre nella figura \ref{fig:SchemaLogicoDatasetIncidentiOperazionale} viene illustrato lo schema logico-relazionale completo del dataset \texttt{Incidenti Stradali} con la notazione offerta da \textit{MySQL Workbench}. \uppercase{è} possibile osservare che in tutte le relazioni definite sono stati inseriti due attributi chiamati \textit{insertTime} e \textit{updateTime} che rispettivamente indicano la data di inserimento e di aggiornamento di uno specifico record in una relazione.

\subsubsection{Pedoni}

\texttt{Pedoni} (\textbf{\underline{idPedone}}, annoNascita, sesso, tipoPersona, tipoLesione, deceduto, decedutoDopo, \textit{idProtocollo})

\'E possibile sottolineare che la chiave primaria della relazione \texttt{Pedoni} non è presente nel dataset scaricato. Per questo motivo è stato deciso di introdurre in questo punto del progetto la chiave primaria \textit{idPedone}, un numero intero auto-incrementale per soddisfare i vincoli di chiave. La chiave esterna \textit{idProtocollo}, che rappresenta il codice identificativo dell'incidente, permette di associare all'incidente lo specifico pedone coinvolto. Concettualmente, un pedone può essere coinvolto in più incidenti ed un incidente può coinvolgere più pedoni. L'impossibilità di distinguere un pedone univocamente porta a modellare la relazione \textit{molti-a-molti} come una relazione \textit{uno-a-molti}, in cui un pedone è associato ad un solo incidente.

\subsubsection{Persone in Auto}

\texttt{PersoneInAuto} (\textbf{\underline{idPersonaInAuto}}, annoNascita, sesso, tipoPersona, tipoLesione, deceduto, decedutoDopo, airbag, cinturaCascoUtilizzato, progressivo, \textit{idProtocollo}, \textit{idVeicolo})

\'E possibile sottolineare che la chiave primaria della relazione \texttt{PersoneInAuto} non è presente nel dataset scaricato. Per questo motivo è stato deciso di introdurre in questo punto del progetto la chiave primaria \textit{idPersonaInAuto}, un numero intero auto-incrementale per soddisfare i vincoli di chiave. La chiave esterna \textit{idProtocollo}, che rappresenta il codice identificativo dell'incidente, permette di associare all'incidente la specifica persona in auto coinvolta. La chiave esterna \textit{idVeicolo}, che rappresenta il codice identificativo del veicolo, permette di associare al veicolo la persona in auto (sia che si tratti del conducente o del passeggero). Concettualmente, una persona in auto può essere coinvolta in più incidenti ed un incidente può coinvolgere più persone in auto. L'impossibilità di distinguere una persona univocamente porta a modellare la relazione \textit{molti-a-molti} come una relazione \textit{uno-a-molti}, in cui una persona in auto è associata ad un solo incidente.

\subsubsection{Veicoli}

\texttt{Veicoli} (\textbf{\underline{idVeicolo}}, marca, modello, statoVeicolo, tipoVeicolo, progressivo, \textit{idProtocollo})

La chiave primaria della relazione \texttt{Veicoli} è un codice numerico univoco già presente nel dataset scaricato. Per questo motivo non è stato necessario introdurre una chiave primaria surrogata, come nel caso dei pedoni o delle persone in auto, ma si è deciso di utilizzare quella disponibile nel dataset scaricato. La chiave esterna \textit{idProtocollo}, che rappresenta il codice identificativo dell'incidente, permette di associare all'incidente lo specifico veicolo coinvolto. Concettualmente, un veicolo può essere coinvolto in più incidenti ed un incidente può coinvolgere più veicoli. Si è notato però che nessun veicolo nel corso degli anni compare più volte. Per questo motivo, la relazione \textit{molti-a-molti} tra \texttt{Veicoli} ed \texttt{Incidenti} è stata modellata come una relazione \textit{uno-a-molti}, in cui un veicolo è associato ad un solo incidente.

\subsubsection{Incidenti}

Il dataset \texttt{Incidenti} presentava al suo interno molte informazioni eterogenee. Si è pensato quindi a dividere queste informazioni identificando le quattro seguenti relazioni:

\texttt{Incidenti} (\textbf{\underline{idIncidente}}, gruppo, dataIncidente, oraIncidente, condizioneAtmosferica, naturaIncidente, numeroRiservata, numeroIllesi, numeroMorti, numeroFeriti, confermato, illuminazione, visibilità, traffico, \textit{idLuogo})

\texttt{Luoghi} (\textbf{\underline{idLuogo}}, latitudine, longitudine, chilometrica, daSpecificare, particolaritàStrade, localizzazione, categoriaStrada, tipoStrada, fondoStradale, pavimentazione, segnaletica)

\texttt{Strade} (\textbf{\underline{idStrada}}, strada)

\texttt{Posizionamento} (\textbf{\textit{\underline{idLuogo}}}, \textbf{\textit{\underline{idStrada}}})

Nello specifico,

\begin{itemize}
	\item la relazione chiamata \texttt{Incidenti} raccoglie informazioni riguardanti uno specifico incidente, come il codice identificativo (un numero intero univoco), la data, l'ora, la condizione atmosferica, la natura dell'incidente, il numero di feriti, illesi, morti e in prognosi riservata coinvolti nell'incidente, l'illuminazione, la visibilità ed il traffico;
	\item la relazione chiamata \texttt{Luoghi} raccoglie informazioni riguardanti una specifica posizione geografica dove è avvenuto uno specifico incidente, come la latitudine, la longitudine, la particolarità della strada, la localizzazione, la categoria di strada, il tipo di strada, il fondo stradale, la pavimentazione e la segnaletica presente;
	\item la relazione chiamata \texttt{Strade} riporta il nome delle strade in cui sono avvenuti uno o più incidenti;
	\item la relazione chiamata \texttt{Posizionamento} rappresenta una relazione \textit{molti-a-molti} tra la relazione \texttt{Luoghi} e \texttt{Strade}. Infatti, ad un luogo specifico possono corrispondere più strade, mentre ad una strada specifica possono corrispondere più luoghi.
\end{itemize}

\uppercase{è} possibile notare che le chiavi primarie delle relazioni \texttt{Luoghi} e \texttt{Strade} non sono già presenti nel dataset scaricato. Per questo motivo è stato deciso di introdurre in questo punto del progetto la chiave primaria \textit{idLuogo} per la relazione \texttt{Luoghi} e la chiave primaria \textit{idStrada} per la relazione \texttt{Strade}, entrambi numeri interi auto-incrementali per rispettare i vincoli di chiave. La chiave esterna \textit{idLuogo} nella relazione \texttt{Incidenti}, che rappresenta il codice identificativo del luogo,  permette di associare all'incidente il luogo dove è avvenuto. Data l'eterogeneità ed il numero di attributi della relazione \texttt{Luoghi} che descrivono l'incidente, la relazione tra \texttt{Luoghi} ed \texttt{Incidenti} è stata modellata come una relazione \textit{uno-a-uno}.

Infine, sono state effettuate due differenti operazioni:
\begin{itemize}
	\item è stato suddiviso in due attributi differenti la data e l'ora di un incidente nella relazione \texttt{Incidenti}, chiamati rispettivamente \textit{dataIncidente} e \textit{oraIncidente} (nel dataset scaricato questi due attributi erano uniti in un solo attributo chiamato \textit{dataOraIncidente});
	\newpage
	\item sono stati rinominati i campi \textit{localizzazione1}, che descrive la tipologia di strada (urbana, extraurbana, \ldots) e \textit{localizzazione2}, che descrive il punto in cui è avvenuto l'incidente (all'intersezione, in prossimità del civico, \ldots) presenti nel dataset originale in \textit{categoriaStrada} e \textit{localizzazione} della relazione \texttt{Luoghi} rispettivamente.
\end{itemize}

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.85\linewidth]{Immagini/SchemaLogicoIncidentiStradali_Operazionale}
	\caption{Schema logico-relazionale del dataset \texttt{Incidenti Stradali}.}
	\label{fig:SchemaLogicoDatasetIncidentiOperazionale}
\end{figure}

\subsection{Schema Concettuale}

La figura \ref{fig:SchemaConcettualeIncidenti} illustra lo schema concettuale ottenuto a partire dallo schema logico-relazionale tramite \textit{reverse engineering} in cui è possibile notare:

\begin{itemize}
	\item una generalizzazione tra le entità \texttt{Pedone} e \texttt{Persona in Auto}. L'entità padre, chiamata \texttt{Persona}, viene descritta tramite tutti gli attributi in comune tra le due relazioni (oltre ad un identificatore univoco chiamato \textit{idPersona}). Inoltre, l'entità figlia \texttt{Persona in Auto} presenta tre attributi che non fanno parte dell'entità \texttt{Pedone};
	\item due associazioni \textit{uno-a-molti} tra le entità \texttt{Pedone} e \texttt{Persona in Auto} con l'entità \texttt{Incidente} che rappresentano il fatto che uno specifico pedone oppure una specifica persona in auto è coinvolta in un solo specifico incidente, mentre in uno specifico incidente possono essere coinvolti zero o più pedoni ed una o più persone in auto;
	\item un'associazione \textit{uno-a-molti} tra le entità \texttt{Persona in Auto} e \texttt{Veicolo} che indica il fatto che una specifica persona in auto è presente in uno specifico veicolo, mentre in uno specifico veicolo possono essere presenti più persone in auto;
	\item un'associazione \textit{uno-a-molti} tra le entità \texttt{Veicolo} e \texttt{Incidente} che rappresenta il fatto che uno specifico veicolo è coinvolto in un incidente, mentre in uno specifico incidente possono essere coinvolti più veicoli;
	\item un'associazione \textit{uno-a-uno} tra le entità \texttt{Incidente} e \texttt{Luogo} che indica il fatto che uno specifico incidente avviene in un luogo particolare ed in un luogo specifico può avvenire un solo incidente;
	\item un'associazione \textit{molti-a-molti} tra le entità \texttt{Luogo} e \texttt{Strada} che rappresenta il fatto che una specifica strada può essere luogo di più incidenti ed uno specifico incidente avvenuto in un luogo può riferirsi a più strade.
\end{itemize}

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.8\textwidth]{Immagini/SchemaConcettualeIncidenti}
	\caption{Schema concettuale del dataset Incidenti.}
	\label{fig:SchemaConcettualeIncidenti}
\end{figure}

\subsection{Manipolazione dei Dati}

\label{section:ManipolazioneDatiIncidenti}

Una volta scaricati i dataset in formato JSON, è stato utilizzato un tool online chiamato \textit{JSON to CSV Converter}\footnote{\url{http://www.convertcsv.com/json-to-csv.htm}} che ha permesso in modo semplice e veloce di trasformare i dataset del caso di studio scaricati in formato \texttt{.csv}\footnote{\url{https://tools.ietf.org/html/rfc4180}} (acronimo di comma-separated values). Questo ha permesso una più semplice visualizzazione e successiva elaborazione dei dati.

Una volta ottenuti i file in formato CSV, sono state implementate diverse classi \texttt{java} allo scopo di separare le informazioni in modo da rendere più semplice e veloce la fase successiva di caricamento dei dati nei database relazionali. L'algoritmo generale che è stato definito per creare questi file è il seguente:

\begin{enumerate}
	\item lettura del file dalla quale è possibile ricavare informazioni di interesse;
	\item selezione delle sole informazioni che descrivono l'entità che si vuole creare;
	\item scrittura su file in formato CSV delle informazioni selezionate nello step precedente che rappresentano l'entità.
\end{enumerate}

\subsection{Caricamento dei Dati in un Database Relazionale}

Una volta creati questi file in formato CSV ottenuti applicando le operazioni descritte nella sottosezione \ref{section:ManipolazioneDatiIncidenti}, è stato possibile caricare i dati all'interno del database relazionale \textit{MySQL}\footnote{\url{https://www.mysql.it}}. Per effettuare questo compito, è stato definito uno \texttt{script SQL} che esegue i seguenti passi:
\begin{enumerate}
	\item creazione di un database relazionale chiamato \texttt{Incidenti\_DB} che conterrà le informazioni relative agli incidenti;
	\item definizione delle relazioni chiamate \texttt{Incidenti, PersoneInAuto, Veicoli, Pedoni, Luoghi, Strade} e \texttt{Luoghi\_Strade} presenti nel database creato in precedenza, specificando gli attributi (nome e tipologia), le chiavi primarie, le chiavi esterne e la tipologia di motore MySQL (InnoDB\footnote{\url{https://dev.mysql.com/doc/refman/5.7/en/innodb-storage-engine.html}});
	\item caricamento dei dati all'interno delle relazioni definite nello step precedente tramite il comando \texttt{LOAD DATA LOCAL INFILE\footnote{\url{https://dev.mysql.com/doc/refman/5.7/en/load-data.html}}}, specificando quale file in formato CSV si vuole caricare, in quale relazione andare a inserire uno specifico insieme di dati, il carattere separatore (nel nostro caso, il simbolo \textquotedblleft{};\textquotedblright{}) ed il carattere di fine linea (nel nostro caso, il simbolo \textquotedblleft{}$\backslash$n\textquotedblright{}).
\end{enumerate}

Per maggiori dettagli, si faccia riferimento allo script\footnote{Script\_CreazionePopolamento\_DatasetIniziali.sql} presente nella cartella \texttt{/ScriptSQL}.

\section{Sorgente Dati: Meteo}

\subsection{Reperimento dei Dati}

Le informazioni relative alla condizione meteorologica del comune di Roma sono riportate nell'archivio storico del sito Web meteorologico italiano \textit{Il Meteo}\footnote{\url{http://www.ilmeteo.it/portale/archivio-meteo/Roma}}. Queste informazioni sono state acquisite tramite l'implementazione di una classe \texttt{java} che effettua le seguenti operazioni:
\begin{enumerate}
	\item reperimento delle pagine Web tramite una fase di \textit{crawling};
	\item \textit{parsing} delle informazioni contenute in ognuno degli indirizzi URL salvati precedentemente, utilizzando la libreria \texttt{JSoup}\footnote{\url{https://jsoup.org}};
	\item salvataggio delle informazioni parsate in file con formato \texttt{.csv}. 
\end{enumerate}

\uppercase{è} stata effettuata un'integrazione dei dati aggiungendo il numero di millimetri d'acqua caduti nei giorni di pioggia, utilizzando come fonte l'archivio storico del sito Web meteorologico italiano \textit{3BMeteo}\footnote{\url{https://www.3bmeteo.com}}. I dati disponibili nei due archivi riportano le informazioni del meteo a livello giornaliero. Non sono stati resi disponibili le condizioni meteo a livello orario. 

\subsection{Analisi del Dataset}

\label{subsection:AnalisiDatasetMeteo}

Il dataset che descrive il meteo permette di ottenere informazioni sulle condizioni atmosferiche del comune di Roma nel periodo temporale 2010 - 2016. In particolare, è possibile avere a disposizione:
\begin{itemize}
	\item la data della rilevazione del meteo (in formato AAAA-MM-GG);
	\item la temperatura minima, massima e media (in gradi Celsius, $^{\circ}$C) rilevata a Roma in una data specifica;
	\item le precipitazioni (in millimetri) rilevate a Roma in una data specifica;
	\item l'umidità (in percentuale, \%) rilevata a Roma in una data specifica;
	\item la velocità massima (in chilometri all'ora - km/h) del vento rilevata a Roma in una data specifica;
	\item la raffica di vento (in chilometri all'ora - km/h) rilevata a Roma in una data specifica;
	\item i fenomeni meteorologici (pioggia, temporale, nebbia, neve, \ldots) rilevati a Roma in una data specifica.
\end{itemize}

\subsection{Schema Logico-Relazionale}

\label{section:SchemaLogicoRelazionaleDatasetMeteo}

\texttt{Meteo} (\textbf{\underline{data}}, temperaturaMinima, temperaturaMassima, temperaturaMedia, precipitazioni, raffica, umidità, fenomeni, ventoMassimo)

Lo schema logico-relazionale del dataset, illustrato nella figura \ref{fig:SchemaLogicoMeteo_Operazionale} con la notazione offerta da \textit{MySQL Workbench}, non è stato fornito insieme ai dati e non è stato possibile ottenerlo da altre fronti. Si è scelto di utilizzare la data di rilevazione delle condizioni meteo come chiave primaria della relazione, in quanto la granularità delle informazioni memorizzate risulta essere giornaliera. Non si hanno quindi due rilevazioni del meteo nello stesso giorno e questo garantisce i vincoli di chiave. Inoltre, ha permesso una facile integrazione con la relazione \texttt{Incidenti} applicando la condizione di \textit{join} tra le due relazioni sul campo \textit{data}, ottenendo quindi informazioni sulla condizione meteo nel momento in cui è successo un incidente. Inoltre, nella relazione \texttt{Meteo}, sono stati inseriti due attributi chiamati \textit{insertTime} e \textit{updateTime} che rispettivamente indicano la data di inserimento e di aggiornamento di uno specifico record nella relazione \texttt{Meteo}.

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.3\linewidth]{Immagini/SchemaLogicoMeteo_Operazionale}
	\caption{Schema logico-relazionale del dataset \texttt{Meteo}.}
	\label{fig:SchemaLogicoMeteo_Operazionale}
\end{figure}

\begin{comment}
\section{Schemi Concettuali delle Sorgenti Dati}

In questa sezione, si illustrano gli schemi concettuali delle due sorgenti dati oggetto del caso di studio, utilizzando i \textit{diagrammi Entity-Relationship} (o diagrammi E/R). Sarà possibile individuare facilmente gli identificatori di ciascuna entità (in grassetto e sottolineati), gli attributi che descrivono ciascuna entità e le associazioni che sussistono tra due o più entità.

Per ottenere gli schemi concettuali delle due sorgenti dati, si è effettuato un processo di \textit{reverse engineering} a partire dagli schemi logici-relazionali presentati e discussi nella sezione \ref{section:SchemiLogiciRelazionaliSorgentiDati}.
\end{comment}

\subsection{Schema Concettuale}

Lo schema concettuale del dataset, ottenuto tramite \textit{reverse engineering} a partire dallo schema logico-relazionale, viene presentato nella figura \ref{fig:SchemaConcettualeMeteo}. 

\begin{figure}[h!]
	\centering
	\includegraphics[scale=0.65]{Immagini/SchemaConcettualeMeteo}
	\caption{Schema concettuale del dataset Meteo.}
	\label{fig:SchemaConcettualeMeteo}
\end{figure}

\uppercase{è} possibile osservare che il nome dell'entità è \texttt{Meteo} e che l'identificatore dell'entità è la data di rilevazione delle informazioni sul meteo. Inoltre, ritroviamo come attributi dell'entità tutti i campi presenti nello schema logico-relazionale della relazione \texttt{Meteo} descritti nella sottosezione \ref{section:SchemaLogicoRelazionaleDatasetMeteo}.

\subsection{Caricamento dei Dati in un Database Relazionale}

Si è scelto di caricare i dati all'interno del database relazionale \textit{MySQL} analogamente al dataset precedente. Per effettuare questo compito, è stato definito uno \texttt{script SQL} che esegue i seguenti passi:
\begin{enumerate}
	\item creazione di un database relazionale chiamato \texttt{Meteo\_DB} che conterrà le informazioni relative alle condizioni meteorologiche di Roma;
	\item definizione della relazione chiamata \texttt{Meteo} presente nel database creato in precedenza, specificando gli attributi (nome e tipologia), le chiavi primarie, le chiavi esterne e la tipologia di motore MySQL (InnoDB);
	\item caricamento dei dati all'interno delle relazioni definite nello step precedente tramite il comando \texttt{LOAD DATA LOCAL INFILE}, specificando quale file in formato CSV si vuole caricare, in quale relazione andare a caricare uno specifico insieme di dati, il carattere separatore (nel nostro caso, il simbolo \textquotedblleft{};\textquotedblright{}) ed il carattere di fine linea (nel caso specifico, il simbolo \textquotedblleft{}$\backslash$n\textquotedblright{}).
\end{enumerate}

Per maggiori dettagli, si faccia riferimento allo script\footnote{Script\_CreazionePopolamento\_DatasetIniziali.sql} presente nella cartella \texttt{/ScriptSQL}.

\section{Analisi dei Requisiti}


La fase di \textit{analisi dei requisiti} ha come obiettivo finale quello di raccogliere le esigenze di utilizzo del data warehouse espresse dai suoi utenti finali. Ha un'importanza strategica poiché influenza le decisioni da prendere riguardo lo schema concettuale dei dati, il progetto dell'alimentazione, le specifiche delle applicazioni per l'analisi dei dati, l'architettura del sistema, il piano di avviamento e di formazione e le linee guida per la manutenzione e l'evoluzione del sistema. La fonte principale da cui attingere i requisiti sono i futuri utenti del data warehouse, mentre per gli aspetti più tecnici saranno gli amministratori del sistema informativo e/o i responsabili del CED (acronimo di Centro Elaborazione Dati) a fungere da riferimento per il progettista.

\subsection{Interviste}

In questa fase, gli utenti vengono coinvolti tramite interviste al fine di delineare le caratteristiche del sistema che si deve progettare. \uppercase{è} necessario quindi tenere in considerazione l'importanza di definire con precisione il livello di storicizzazione del fatto, ovvero l'arco temporale che gli eventi memorizzati devono coprire, e le dimensioni che lo descrivono poiché permette di determinare la granularità del fatto.

\begin{table}[h!]
	\centering
	\resizebox{\textwidth}{!}{%
		\begin{tabular}{|c|l|}
			\hline
			\textbf{Ruolo} & \multicolumn{1}{c|}{\textbf{Domande chiave}} \\ \hline
			Sindaco & \begin{tabular}[c]{@{}l@{}}1) Perché ritiene utile la realizzazione di\\ un data warehouse che analizza gli incidenti?\\ 2) In che modo si aspetta che una maggiore disponibilità \\di informazioni possa migliorare la gestione del comune?\\ 3) Quali sono gli obiettivi del comune?\\ 4) Quali sono le spese maggiori causate dagli incidenti?\\ 5) Con quale frequenza vuoi disporre delle informazioni?\end{tabular} \\ \hline
			\begin{tabular}[c]{@{}c@{}}Direttore del dipartimento\\ \textquotedblleft{}Mobilità e Trasporti\textquotedblright{}\end{tabular} & \begin{tabular}[c]{@{}l@{}}1) Quali sono i principali problemi relativi al traffico?\\ 2) Quali sono gli obiettivi del suo dipartimento?\\ 3) Come misura il successo del suo dipartimento?\\ 4) Di quali tipologie di analisi vuole disporre \\ ed a che livello di dettaglio?\\ 5) Su quale intervallo temporale vuole condurre analisi?\\ 6) Con quale frequenza vuole disporre delle informazioni?\end{tabular} \\ \hline
			\begin{tabular}[c]{@{}c@{}}Responsabile dei\\ Sistemi Informativi\end{tabular} & \begin{tabular}[c]{@{}l@{}}1) Dove sono memorizzati i dati di interesse?\\ 2) Quali strumenti vengono utilizzati per condurre analisi?\\ 3) Con quale cadenza devono essere generati i report?\\ 4) Quali sono le problematiche relative alla qualità dei dati?\\ 5) Come è organizzato il suo settore IT?\\ 6) Disponete di un team competente per la gestione ed\\ il mantenimento nel tempo del data warehouse?\end{tabular} \\ \hline
			Cittadino & \begin{tabular}[c]{@{}l@{}}1) Pensa che possa essere utile avere informazioni\\ sulla distribuzione del traffico e degli incidenti?\\ 2) Se i report generati fossero resi disponibili\\ in modalità open, li consulterebbe?\end{tabular} \\ \hline
		\end{tabular}%
	}
	\caption{Interviste agli utenti che hanno parte attiva nel progetto.}
	\label{tab:IntervisteUtenti}
\end{table}

Gli utenti identificati come entità partecipanti al progetto ed utilizzatori finali del sistema sono i seguenti:
\begin{itemize}
	\item il \textit{comune di Roma}, in quanto è il committente della realizzazione del data warehouse. Inoltre è parte attiva nella gestione degli incidenti dal punto di vista delle risorse (economiche e del personale) coinvolte. Le domande dell'intervista sono state rivolte al sindaco ed al suo portavoce;
	\item il \textit{dipartimento \textquotedblleft Mobilità e Trasporti\textquotedblright{}}\footnote{\url{http://www.comune.roma.it/pcr/it/dipartimento_mob_trasp.page}} del comune di Roma, in quanto è l'organo incaricato della gestione non solo del traffico, a cui spesso viene ridotto, ma un sistema complesso che comprende tutto ciò che è in relazione al muoversi, con qualsiasi mezzo, nella città e nel territorio. Le domande dell'intervista sono state rivolte al direttore di tale dipartimento;
	\item i \textit{sistemi informativi} del comune di Roma, al fine di ottenere informazioni sui dati presenti, la loro gestione e la struttura dell'organizzazione del sistema informativo. Le domande dell'intervista sono state rivolte al responsabile dei sistemi informativi;
	\item un \textit{campione di cittadini} del comune di Roma, in quanto parte attiva coinvolta negli incidenti e nel traffico della capitale d'Italia.
\end{itemize}

Nella tabella \ref{tab:IntervisteUtenti} vengono riportate le domande poste agli utenti appena definiti.

\subsection{Glossario dei Requisiti}

Il \textit{glossario dei requisiti} rappresenta un risultato dell'analisi dei requisiti utente e permette di identificare i fatti di interesse di uno specifico dominio. Ciascun fatto è caratterizzato da possibili dimensioni, da possibili misure e dalla storicità.

Nella tabella \ref{tab:GlossarioRequisiti} si illustra il glossario dei requisiti prodotto tramite la fase di analisi dei requisiti le cui informazioni sono state acquisite mediante un'interazione con gli utenti finali del data warehouse.

\begin{table}[h!]
	\centering
	\resizebox{\textwidth}{!}{
		\begin{tabular}{|c|c|c|c|}
			\hline
			{\textit{\textbf{Fatto}}} & {\textit{\textbf{Possibili dimensioni}}} & {\textit{\textbf{Possibili misure}}} & {\textit{\textbf{Storicità}}} \\ \hline
			Incidente & \begin{tabular}[c]{@{}c@{}}Data in cui è avvenuto l'incidente,\\ Orario in cui è avvenuto l'incidente,\\ Posizione geografica dove è avvenuto l'incidente,\\ Veicoli coinvolti nell'incidente,\\ Pedoni coinvolti nell'incidente,\\ Persone in automobile coinvolte nell'incidente,\\ Condizione meteorologica al momento dell'incidente,\\Condizioni aggiuntive al momento dell'incidente\end{tabular} & \begin{tabular}[c]{@{}c@{}}Numero di incidenti,\\Numero di feriti,\\ Numero di morti,\\ Numero di illesi,\\Numero di persone \\ in prognosi riservata\end{tabular} & 7 anni \\ \hline
		\end{tabular}
	}
	\caption{Glossario dei requisiti.}
	\label{tab:GlossarioRequisiti}
\end{table}

\subsection{Stima del Carico di Lavoro Preliminare}

\label{subsection:StimaCaricoLavoroPreliminare}

Il riconoscimento dei fatti, delle dimensioni e delle misure è strettamente collegato all'identificazione del \textit{carico di lavoro preliminare}. In questa fase, il carico di lavoro può essere espresso in linguaggio naturale; esso sarà comunque utile per valutare la granularità dei fatti e le misure di interesse, nonché per iniziare ad affrontare il problema dell'aggregazione.

Nella tabella \ref{tab:StimaCaricoLavoroPreliminare} si mostra la stima del carico di lavoro preliminare prodotto tramite la fase di analisi dei requisiti le cui informazioni sono state acquisite mediante un'interazione con gli utenti finali del data warehouse.

\begin{table}[h!]
	\centering
	\resizebox{\textwidth}{!}{%
		\begin{tabular}{|l|l|}
			\hline
			\multicolumn{1}{|c|}{\textbf{Fatto}} & \multicolumn{1}{c|}{\textbf{Interrogazione}} \\ \hline
			\multirow{5}{*}{Incidente} & 1) Distribuzione degli incidenti in base alle condizioni meteorologiche \\ \cline{2-2} 
			& 2) Confronto annuale degli incidenti avvenuti per ogni fascia oraria del giorno \\ \cline{2-2} 
			& 3) Studio della distribuzione geografica degli incidenti\\ \cline{2-2} 
			& 4) Andamento degli incidenti (mortali e non mortali) per ciascun anno \\ \cline{2-2} 
			& 5) Studio comparativo annuale sulla tipologia dei veicoli coinvolti negli incidenti \\ \hline
		\end{tabular}%
	}
	\caption{Stima del carico di lavoro preliminare.}
	\label{tab:StimaCaricoLavoroPreliminare}
\end{table}

\section{Valutazione della Qualità dei Dati}

\label{section:ValutazioneQualitàDati}

Dall'analisi effettuata sul dataset degli incidenti descritto nella sottosezione \ref{subsection:AnalisiDatasetIncidenti} sono emersi alcuni problemi relativi sia alla struttura dei dati sia al contenuto stesso. 
Alcuni file, in formato JSON, presenti sul portale del comune di Roma relativi ai veicoli e agli incidenti, sono risultati essere JSON malformati a causa della presenza di campi al cui interno è stato utilizzato il simbolo \textquotedblleft{} (virgolette) per definire luoghi o ristoranti oppure altezze specifiche di Roma (ad esempio, il ristorante \textquotedblleft{}Montarozzo\textquotedblright{}). Questo ha generato un conflitto con la sintassi di tale linguaggio ed è stato quindi necessario operare una correzione per la successiva fase di conversione in formato CSV. \\
In merito al contenuto dei dati, sono stati rilevati i seguenti problemi:

\begin{itemize}
	\item incongruenza, a livello generale, nello stesso anno o nel passaggio da un anno all'altro, nel formato e nella notazione utilizzata per descrivere lo stesso concetto. Ad esempio, l'attributo \textit{deceduto} relativo ad una persona compare come \textquotedblleft\textit{sì/no/in seguito/dopo}\textquotedblright, \textquotedblleft\textit{0/1}\textquotedblright,\textquotedblleft\textit{true/TRUE/VERO/false/FALSE/ FALSO}\textquotedblright. \uppercase{è} stato necessario operare un processo di normalizzazione di tali informazioni ai fini di analisi future;
	
	\item molte informazioni, soprattutto negli anni dal 2010 al 2014, risultano essere mancanti. Questo è imputabile ad un aggiornamento in corso d'opera dei sistemi che hanno permesso di rilevare informazioni aggiuntive, come ad esempio la posizione geografica espressa in coordinate GPS;

	\item  le informazioni relative alle persone in auto e ai pedoni coinvolti non ha permesso di identificare univocamente l'individuo nel tempo. Inoltre, alcuni pedoni presentavano una data di nascita superiore all'anno corrente. Si è operata un'eliminazione di tali record;
	
	\item analizzando il numero di protocollo degli incidenti è emerso una grande sproporzione tra il numero di veicoli coinvolti negli incidenti e il numero di persone in auto riportato. Sebbene una differenza tra queste due grandezze sia lecita (ad esempio, nel caso di incidenti che coinvolgono auto in sosta), dall'analisi compiuta è risultato che circa il 75\% dei veicoli riportati non ha una persona associata.
\end{itemize}
