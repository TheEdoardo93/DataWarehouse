package processoElaborazioneDataset;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/* Questa classe permette di creare un file in formato .csv che contiene tutte le informazioni per la tabella "Luoghi".
 */
public class CreazioneTabellaLuoghi {

	public static void main(String[] args) {
		/*Percorso del file .csv che contiene gli incidenti dell'anno 2016.*/
		String csvFile_incidenti = "Incidenti_gen_dic_2010.csv";
		
		/*Lettura del file .csv relativo agli incidenti e memorizzo in una struttura dati (ArrayList) i dati letti dal file .csv.*/
		BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ";";
        
        try {
			br = new BufferedReader(new FileReader(csvFile_incidenti));
		} catch (FileNotFoundException e) {
			System.out.println("Il file .csv che descrive gli incidenti del 2015 non č stato trovato!");
			e.printStackTrace();
		}
        
        int numero_record_luoghi = 0; //numero di record (corrispondente al numero di incidenti presenti nel file .csv)
        ArrayList <String> vettore_latitudine = new ArrayList <String>();
        ArrayList <String> vettore_longitudine = new ArrayList<String>();
        ArrayList <String> vettore_chilometrica = new ArrayList<String>();
        ArrayList <String> vettore_da_specificare = new ArrayList<String>();
        ArrayList <String> vettore_particolaritā_strade = new ArrayList<String>();
        ArrayList <String> vettore_localizzazione = new ArrayList<String>();
        ArrayList <String> vettore_categoria_strada = new ArrayList<String>();
        ArrayList <String> vettore_tipo_strada = new ArrayList<String>();
        ArrayList <String> vettore_fondo_stradale = new ArrayList<String>();
        ArrayList <String> vettore_pavimentazione = new ArrayList<String>();
        ArrayList <String> vettore_segnaletica = new ArrayList<String>();
        
        try {
			while ((line = br.readLine()) != null) {
				String[] stringa = line.split(cvsSplitBy);
				
				if ((!(stringa[0].equalsIgnoreCase("id"))) && (!(stringa[1].equalsIgnoreCase("gruppo")))) { //se la riga letta dal file .csv non č l'header, la considero
					numero_record_luoghi++;
					vettore_latitudine.add(stringa[26]);
					vettore_longitudine.add(stringa[25]);
					vettore_chilometrica.add(stringa[9]);
					vettore_da_specificare.add(stringa[10]);
					vettore_particolaritā_strade.add(stringa[12]);
					vettore_localizzazione.add(stringa[5]);
					vettore_categoria_strada.add(stringa[4]);
					vettore_tipo_strada.add(stringa[13]);
					vettore_fondo_stradale.add(stringa[14]);
					vettore_pavimentazione.add(stringa[15]);
					vettore_segnaletica.add(stringa[16]);
					}
			}
		} catch (NumberFormatException e) {
			System.out.println("Problemi di NumberFormatException!");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("Problemi di IOException!");
			e.printStackTrace();
		}
        
        System.out.println("Il numero di incidenti letti č " + numero_record_luoghi); //non compreso l'header del file .csv   
        
        /*Scrittura su file .csv del risultato finale ottenuto tramite l'algoritmo*/
    	
    	FileWriter writer = aperturaFileOutputCSV();
    	
    	salvataggioSuFileCSV(writer, vettore_latitudine, vettore_longitudine, vettore_chilometrica, vettore_da_specificare, vettore_particolaritā_strade, vettore_localizzazione,
    			vettore_categoria_strada, vettore_tipo_strada, vettore_fondo_stradale, vettore_pavimentazione, vettore_segnaletica, numero_record_luoghi);
    	
    	chiusuraFileOutputCSV(writer);
    }

	private static void chiusuraFileOutputCSV(FileWriter writer) {
		try {
	        writer.flush();
	        writer.close();
	        
	    } catch (IOException e) {
	        System.out.println("Errore nel flush e/o nella chiusura del writer!");
	        e.printStackTrace();
	    }
	}
	
	private static FileWriter aperturaFileOutputCSV() {
		String csv_header = "idLuogo;latitudine;longitudine;chilometrica;daSpecificare;particolaritāStrade;"
				+ "localizzazione;categoriaStrada;tipoStrada;fondoStradale;pavimentazione;segnaletica";
		FileWriter writer = null;
		
		try {
			writer = new FileWriter("prova.csv");
			writer.append(csv_header.toString());
		} catch (Exception e) {
	        System.out.println("Errore nella creazione del file di output in formato .csv!");
	        e.printStackTrace();
		}	
		return writer;
		
	}
	
	private static void salvataggioSuFileCSV(FileWriter writer, ArrayList<String> vettore_latitudine, ArrayList<String> vettore_longitudine, ArrayList<String> vettore_chilometrica,
			ArrayList<String> vettore_da_specificare, ArrayList<String> vettore_particolaritā_strade, ArrayList<String> vettore_localizzazione,
			ArrayList<String> vettore_categoria_strada, ArrayList<String> vettore_tipo_strada, ArrayList<String> vettore_fondo_stradale, ArrayList<String> vettore_pavimentazione,
			ArrayList<String> vettore_segnaletica, int numero_record_luoghi) {
		
		int id_incrementale = 168802;
		
		try {
			for (int i = 0; i < numero_record_luoghi; i++) {
				writer.append("\n");
				writer.append(String.valueOf(id_incrementale));
				writer.append(";");
				writer.append(String.valueOf(vettore_latitudine.get(i)));
				writer.append(";");
				writer.append(String.valueOf(vettore_longitudine.get(i)));
				writer.append(";");
				writer.append(String.valueOf(vettore_chilometrica.get(i)));
				writer.append(";");
				writer.append(String.valueOf(vettore_da_specificare.get(i)));
				writer.append(";");
				writer.append(String.valueOf(vettore_particolaritā_strade.get(i)));
				writer.append(";");
				writer.append(String.valueOf(vettore_localizzazione.get(i)));
				writer.append(";");
				writer.append(String.valueOf(vettore_categoria_strada.get(i)));
				writer.append(";");
				writer.append(String.valueOf(vettore_tipo_strada.get(i)));
				writer.append(";");
				writer.append(String.valueOf(vettore_fondo_stradale.get(i)));
				writer.append(";");
				writer.append(String.valueOf(vettore_pavimentazione.get(i)));
				writer.append(";");
				writer.append(String.valueOf(vettore_segnaletica.get(i)));
				
				id_incrementale++;
			}
		} catch (Exception e) {
			System.out.println("Errore nella scrittura del file di output in formato .csv!");
			e.getMessage();
		}
		
		System.out.println("\n");
		System.out.println("Tabella Luoghi correttamente creato in un file di output in formato .csv!");
	}
	
	}
