package processoElaborazioneDataset;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class TabellaNN {

	static Integer riga_inc =0;
	static Integer strade_inc =34881;
	public static void main(String[] args) throws IOException {

		File incidenti = new File("2016/");
		File strade = new File("tmpout/Strade/");


		String[] listOfFilesIncidenti = incidenti.list();
		String[] listOfFilesStrade = strade.list();

		
		ArrayList<String> tabella_nxn = new ArrayList<String>();

		for(int i=0; i<listOfFilesIncidenti.length ;i++){
			
			ArrayList<String> elenco_strade = new ArrayList<String>();
			BufferedReader bf_incidente = new BufferedReader(new FileReader(incidenti+"/"+listOfFilesIncidenti[i]));
			BufferedReader bf_strade = new BufferedReader(new FileReader(strade+"/"+listOfFilesStrade[i]));

			bf_strade.readLine(); //tolgo l'header;
			bf_incidente.readLine(); //tolgo l'header;

			String line ="";

			int cont=0;
			while((line=bf_strade.readLine())!=null){
				String [] linea = line.split(";");
				elenco_strade.add(linea[0]);
				cont++;
			}
			
			while((line=bf_incidente.readLine())!=null){
				riga_inc++;
				String [] linea = line.split(";");
				for(int j=0; j<elenco_strade.size(); j++){
					if(linea[7].equalsIgnoreCase(elenco_strade.get(j))){
						if(listOfFilesIncidenti[i].indexOf("gen")!=-1)
							tabella_nxn.add(riga_inc.toString()+";"+ (strade_inc+j+1)+";2016-03-01 00:00:00;2016-03-01 00:00:00\n");
						if(listOfFilesIncidenti[i].indexOf("mar")!=-1)
							tabella_nxn.add(riga_inc.toString()+";"+ (strade_inc+j+1)+";2016-05-01 00:00:00;2016-05-01 00:00:00\n");
						if(listOfFilesIncidenti[i].indexOf("mag")!=-1)
							tabella_nxn.add(riga_inc.toString()+";"+ (strade_inc+j+1)+";2016-07-01 00:00:00;2016-07-01 00:00:00\n");
						if(listOfFilesIncidenti[i].indexOf("lug")!=-1)
							tabella_nxn.add(riga_inc.toString()+";"+ (strade_inc+j+1)+";2016-09-01 00:00:00;2016-09-01 00:00:00\n");
						if(listOfFilesIncidenti[i].indexOf("sett")!=-1)
							tabella_nxn.add(riga_inc.toString()+";"+ (strade_inc+j+1)+";2016-11-01 00:00:00;2016-11-01 00:00:00\n");
						if(listOfFilesIncidenti[i].indexOf("nov")!=-1)
							tabella_nxn.add(riga_inc.toString()+";"+ (strade_inc+j+1)+";2017-01-01 00:00:00;2017-01-01 00:00:00\n");
						break;
					}
				}
				
				if(!(linea[8].equals("")) && (!(linea[8].equalsIgnoreCase(linea[7])))){
					for(int j=0; j<elenco_strade.size(); j++){
						if(linea[8].equals(elenco_strade.get(j))){
							if(listOfFilesIncidenti[i].indexOf("gen")!=-1)
								tabella_nxn.add(riga_inc.toString()+";"+ (strade_inc+j+1)+";2016-03-01 00:00:00;2016-03-01 00:00:00\n");
							if(listOfFilesIncidenti[i].indexOf("mar")!=-1)
								tabella_nxn.add(riga_inc.toString()+";"+ (strade_inc+j+1)+";2016-05-01 00:00:00;2016-05-01 00:00:00\n");
							if(listOfFilesIncidenti[i].indexOf("mag")!=-1)
								tabella_nxn.add(riga_inc.toString()+";"+ (strade_inc+j+1)+";2016-07-01 00:00:00;2016-07-01 00:00:00\n");
							if(listOfFilesIncidenti[i].indexOf("lug")!=-1)
								tabella_nxn.add(riga_inc.toString()+";"+ (strade_inc+j+1)+";2016-09-01 00:00:00;2016-09-01 00:00:00\n");
							if(listOfFilesIncidenti[i].indexOf("sett")!=-1)
								tabella_nxn.add(riga_inc.toString()+";"+ (strade_inc+j+1)+";2016-11-01 00:00:00;2016-11-01 00:00:00\n");
							if(listOfFilesIncidenti[i].indexOf("nov")!=-1)
								tabella_nxn.add(riga_inc.toString()+";"+ (strade_inc+j+1)+";2017-01-01 00:00:00;2017-01-01 00:00:00\n");
							break;
						}
					}
				}
			}
			bf_strade.close();
			bf_incidente.close();
			strade_inc=strade_inc+cont;
		}
		

		System.out.println("Dimensione tabella: " + tabella_nxn.size());
		FileWriter tabella = new FileWriter("tmpout/tabella/tabella_2016.csv", true);
		tabella.append("idLuogo;idStrada;insertTime;updateTime\n");
		tabella.flush();
		for(int i=0; i<tabella_nxn.size();i++){
			tabella.append(tabella_nxn.get(i));
			tabella.flush();
		}
		tabella.close();

	}
}