package processoElaborazioneDataset;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/* Questa classe permette di creare un file in formato .csv che contiene tutte le informazioni per
 * la tabella "Incidenti".
 */
public class CreazioneTabellaIncidenti {

	public static void main(String[] args) {
		/*Percorso del file .csv che contiene gli incidenti dell'anno.*/
		String csvFile_incidenti = "Incidenti_gen_dic_2015.csv";
		
		/*Lettura del file .csv relativo agli incidenti e memorizzo in una struttura dati (ArrayList) i dati letti dal file .csv.*/
		BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ";";
        
        try {
			br = new BufferedReader(new FileReader(csvFile_incidenti));
		} catch (FileNotFoundException e) {
			System.out.println("Il file .csv che descrive gli incidenti non � stato trovato!");
			e.printStackTrace();
		}
        
        int numero_record_incidenti = 0; //numero di record (corrispondente al numero di incidenti presenti nel file .csv)
        ArrayList <Integer> vettore_id_incidenti = new ArrayList <Integer>();
        ArrayList <Integer> vettore_gruppo = new ArrayList <Integer>();
        ArrayList <String> vettore_data_incidente = new ArrayList<String>();
        ArrayList <String> vettore_ora_incidente = new ArrayList<String>();
        ArrayList <String> vettore_condizione_atmosferica = new ArrayList<String>();
        ArrayList <String> vettore_natura_incidente = new ArrayList<String>();
        ArrayList <Integer> vettore_numero_illesi = new ArrayList<Integer>();
        ArrayList <Integer> vettore_numero_morti = new ArrayList<Integer>();
        ArrayList <Integer> vettore_numero_riservata = new ArrayList<Integer>();
        ArrayList <Integer> vettore_numero_feriti= new ArrayList<Integer>();
        ArrayList <String> vettore_confermato = new ArrayList<String>();
        ArrayList <String> vettore_illuminazione = new ArrayList<String>();
        ArrayList <String> vettore_visibilit� = new ArrayList<String>();
        ArrayList <String> vettore_traffico = new ArrayList<String>();
        
        try {
        	
        	br.readLine();
        	
			while ((line = br.readLine()) != null) {
				String[] stringa = line.split(cvsSplitBy);
				
				numero_record_incidenti++;
				vettore_id_incidenti.add(Integer.parseInt(stringa[0]));
				vettore_gruppo.add(Integer.parseInt(stringa[1]));
				vettore_data_incidente.add(stringa[2]);
				vettore_ora_incidente.add(stringa[3]);
				vettore_condizione_atmosferica.add(stringa[17]);
				vettore_natura_incidente.add(stringa[11]);
				vettore_numero_illesi.add(Integer.parseInt(stringa[24]));
				vettore_numero_morti.add(Integer.parseInt(stringa[23]));
				vettore_numero_riservata.add(Integer.parseInt(stringa[22]));
				vettore_numero_feriti.add(Integer.parseInt(stringa[21]));
				vettore_confermato.add(stringa[27]);
				vettore_illuminazione.add(stringa[20]);
				vettore_visibilit�.add(stringa[19]);
				vettore_traffico.add(stringa[18]);
			}
		} catch (NumberFormatException e) {
			System.out.println("Problemi di NumberFormatException!");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("Problemi di IOException!");
			e.printStackTrace();
		}
        
        System.out.println("Il numero di incidenti letti � " + numero_record_incidenti); //non compreso l'header del file .csv   
        
        /*Scrittura su file .csv del risultato finale ottenuto tramite l'algoritmo*/
    	
    	FileWriter writer = aperturaFileOutputCSV();
    	
    	salvataggioSuFileCSV(writer, vettore_id_incidenti, vettore_gruppo, vettore_data_incidente, vettore_ora_incidente, vettore_condizione_atmosferica, vettore_natura_incidente, vettore_numero_illesi,
    			vettore_numero_morti, vettore_numero_riservata, vettore_numero_feriti, vettore_confermato, vettore_illuminazione, vettore_visibilit�, vettore_traffico, numero_record_incidenti);
    	
    	chiusuraFileOutputCSV(writer);
    }

	private static void chiusuraFileOutputCSV(FileWriter writer) {
		try {
	        writer.flush();
	        writer.close();
	        
	    } catch (IOException e) {
	        System.out.println("Errore nel flush e/o nella chiusura del writer!");
	        e.printStackTrace();
	    }
	}
	
	private static FileWriter aperturaFileOutputCSV() {
		String csv_header = "idIncidente;gruppo;dataIncidente;oraIncidente;condizioneAtmosferica;naturaIncidente;"
				+ "numeroRiservata;numeroIllesi;numeroMorti;numeroFeriti;confermato;illuminazione;visibilit�;traffico;idLuogo";
		FileWriter writer = null;
		
		try {
			writer = new FileWriter("prova.csv");
			writer.append(csv_header.toString());
		} catch (Exception e) {
	        System.out.println("Errore nella creazione del file di output in formato .csv!");
	        e.printStackTrace();
		}	
		return writer;
		
	}
	
	private static void salvataggioSuFileCSV(FileWriter writer, ArrayList<Integer> vettore_id_incidenti, ArrayList<Integer> vettore_gruppo, ArrayList<String> vettore_data_incidente,
			ArrayList<String> vettore_ora_incidente, ArrayList<String> vettore_condizione_atmosferica, ArrayList<String> vettore_natura_incidente, ArrayList<Integer> vettore_numero_illesi,
			ArrayList<Integer> vettore_numero_morti, ArrayList<Integer> vettore_numero_riservata, ArrayList<Integer> vettore_numero_feriti, ArrayList<String> vettore_confermato,
			ArrayList<String> vettore_illuminazione, ArrayList<String> vettore_visibilit�, ArrayList<String> vettore_traffico, int numero_record_incidenti) {
		
		int id_luogo_incrementale = 1;
		
		try {
			for (int i = 0; i < numero_record_incidenti; i++) {
				writer.append("\n");
				writer.append(String.valueOf(vettore_id_incidenti.get(i)));
				writer.append(";");
				writer.append(String.valueOf(vettore_gruppo.get(i)));
				writer.append(";");
				writer.append(String.valueOf(vettore_data_incidente.get(i)));
				writer.append(";");
				writer.append(String.valueOf(vettore_ora_incidente.get(i)));
				writer.append(";");
				writer.append(String.valueOf(vettore_condizione_atmosferica.get(i)));
				writer.append(";");
				writer.append(String.valueOf(vettore_natura_incidente.get(i)));
				writer.append(";");
				writer.append(String.valueOf(vettore_numero_illesi.get(i)));
				writer.append(";");
				writer.append(String.valueOf(vettore_numero_morti.get(i)));
				writer.append(";");
				writer.append(String.valueOf(vettore_numero_riservata.get(i)));
				writer.append(";");
				writer.append(String.valueOf(vettore_numero_feriti.get(i)));
				writer.append(";");
				writer.append(String.valueOf(vettore_confermato.get(i)));
				writer.append(";");
				writer.append(String.valueOf(vettore_illuminazione.get(i)));
				writer.append(";");
				writer.append(String.valueOf(vettore_visibilit�.get(i)));
				writer.append(";");
				writer.append(String.valueOf(vettore_traffico.get(i)));
				writer.append(";");
				writer.append(String.valueOf(id_luogo_incrementale));
				
				id_luogo_incrementale++;
			}
		} catch (Exception e) {
			System.out.println("Errore nella scrittura del file di output in formato .csv!");
			e.getMessage();
		}
		
		System.out.println("\n");
		System.out.println("Tabella Incidenti correttamente creato in un file di output in formato .csv!");
	}
	
	}
