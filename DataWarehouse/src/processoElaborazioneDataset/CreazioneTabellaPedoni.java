package processoElaborazioneDataset;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/* Questa classe permette di creare un file in formato .csv che contiene tutte le informazioni per la tabella "Pedoni".
 */
public class CreazioneTabellaPedoni {

	public static void main(String[] args) {
		/*Percorso del file .csv che contiene i pedoni dell'anno 2016.*/
		String csvFile_pedoni = "Pedoni_gen_dic_2015.csv";
		
		/*Lettura del file .csv relativo ai pedoni e memorizzo in una struttura dati (ArrayList) i dati letti dal file .csv.*/
		BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ";";
        
        try {
			br = new BufferedReader(new FileReader(csvFile_pedoni));
		} catch (FileNotFoundException e) {
			System.out.println("Il file .csv che descrive i pedoni del 2016 non � stato trovato!");
			e.printStackTrace();
		}
        
        int numero_record_pedoni = 0; //numero di record (corrispondente al numero di pedoni presenti nel file .csv)
        ArrayList <Integer> vettore_id_incidenti = new ArrayList <Integer>();
        ArrayList <String> vettore_tipo_persona = new ArrayList <String>();
        ArrayList <String> vettore_anno_nascita = new ArrayList <String>();
        ArrayList <String> vettore_sesso = new ArrayList<String>();
        ArrayList <String> vettore_tipo_lesione = new ArrayList<String>();
        ArrayList <String> vettore_deceduto = new ArrayList<String>();
        ArrayList <String> vettore_deceduto_dopo = new ArrayList<String>();
        
        try {
        	
        	br.readLine(); //leggo l'header del file .csv
        	
			while ((line = br.readLine()) != null) {
				String[] stringa = line.split(cvsSplitBy);
				
				numero_record_pedoni++;
				vettore_id_incidenti.add(Integer.parseInt(stringa[0]));
				vettore_tipo_persona.add(stringa[1]);
				vettore_anno_nascita.add(stringa[2]);
				vettore_sesso.add(stringa[3]);
				vettore_tipo_lesione.add(stringa[4]);
				vettore_deceduto.add(stringa[5]);
				if (stringa.length == 6) { //serve per risolvere il problema che l'attributo "decedutoDopo" non contiene nessun valore, quindi stringa.length = 6 al posto di 7
					vettore_deceduto_dopo.add("");
				} else {
					if (stringa.length == 7) {
						vettore_deceduto_dopo.add(stringa[6]);
					}
				}
			}
		} catch (NumberFormatException e) {
			System.out.println("Problemi di NumberFormatException!");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("Problemi di IOException!");
			e.printStackTrace();
		}
        
        System.out.println("Il numero di pedoni letti � " + numero_record_pedoni); //non compreso l'header del file .csv   
        
        /*Scrittura su file .csv del risultato finale ottenuto tramite l'algoritmo*/
    	
    	FileWriter writer = aperturaFileOutputCSV();
    	
    	salvataggioSuFileCSV(writer, vettore_id_incidenti, vettore_tipo_persona, vettore_anno_nascita, vettore_sesso,
    			vettore_tipo_lesione, vettore_deceduto, vettore_deceduto_dopo, numero_record_pedoni);
    	
    	chiusuraFileOutputCSV(writer);
    }

	private static void chiusuraFileOutputCSV(FileWriter writer) {
		try {
	        writer.flush();
	        writer.close();
	    } catch (IOException e) {
	        System.out.println("Errore nel flush e/o nella chiusura del writer!");
	        e.printStackTrace();
	    }
	}
	
	private static FileWriter aperturaFileOutputCSV() {
		String csv_header = "idPedone;tipoPersona;annoNascita;sesso;tipoLesione;deceduto;decedutoDopo;idProtocollo";
		FileWriter writer = null;
		
		try {
			writer = new FileWriter("prova.csv");
			writer.append(csv_header.toString());
		} catch (Exception e) {
	        System.out.println("Errore nella creazione del file di output in formato .csv!");
	        e.printStackTrace();
		}	
		return writer;
		
	}
	
	private static void salvataggioSuFileCSV(FileWriter writer, ArrayList<Integer> vettore_id_incidenti, ArrayList<String> vettore_tipo_persona, ArrayList<String> vettore_anno_nascita,
			ArrayList<String> vettore_sesso, ArrayList<String> vettore_tipo_lesione, ArrayList<String> vettore_deceduto, ArrayList<String> vettore_deceduto_dopo, int numero_record_pedoni) {
		
		int id_pedone_incrementale = 1;
		
		try {
			for (int i = 0; i < numero_record_pedoni; i++) {
				writer.append("\n");
				writer.append(String.valueOf(id_pedone_incrementale));
				writer.append(";");
				writer.append(String.valueOf(vettore_tipo_persona.get(i)));
				writer.append(";");
				writer.append(String.valueOf(vettore_anno_nascita.get(i)));
				writer.append(";");
				writer.append(String.valueOf(vettore_sesso.get(i)));
				writer.append(";");
				writer.append(String.valueOf(vettore_tipo_lesione.get(i)));
				writer.append(";");
				writer.append(String.valueOf(vettore_deceduto.get(i)));
				writer.append(";");
				writer.append(String.valueOf(vettore_deceduto_dopo.get(i)));
				writer.append(";");
				writer.append(String.valueOf(vettore_id_incidenti.get(i)));
				
				id_pedone_incrementale++;
			}
		} catch (Exception e) {
			System.out.println("Errore nella scrittura del file di output in formato .csv!");
			e.getMessage();
		}
		
		System.out.println("\n");
		System.out.println("Tabella Pedoni correttamente creato in un file di output in formato .csv!");
	}
	
	}
