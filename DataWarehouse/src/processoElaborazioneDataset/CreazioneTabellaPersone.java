package processoElaborazioneDataset;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/* Questa classe permette di creare un file in formato .csv che contiene tutte le informazioni per la tabella "PersoneInAuto".
 */
public class CreazioneTabellaPersone {

	public static void main(String[] args) {
		/*Percorso del file .csv che contiene le persone (in auto) dell'anno 2016.*/
		String csvFile_personeInAuto = "Persone_gen_dic_2015.csv";
		
		/*Lettura del file .csv relativo alle persone (in auto) e memorizzo in una struttura dati (ArrayList) i dati letti dal file .csv.*/
		BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ";";
        
        try {
			br = new BufferedReader(new FileReader(csvFile_personeInAuto));
		} catch (FileNotFoundException e) {
			System.out.println("Il file .csv che descrive le persone (in auto) del 2016 non � stato trovato!");
			e.printStackTrace();
		}
        
        int numero_record_persone_in_auto = 0; //numero di record (corrispondente al numero di persone (in auto) presenti nel file .csv)
        ArrayList <Integer> vettore_id_incidenti = new ArrayList <Integer>();
        ArrayList <Integer> vettore_id_veicoli = new ArrayList <Integer>();
        ArrayList <Integer> vettore_progressivo = new ArrayList <Integer>();
        ArrayList <String> vettore_tipo_persona = new ArrayList <String>();
        ArrayList <Integer> vettore_anno_nascita = new ArrayList<Integer>();
        ArrayList <String> vettore_sesso = new ArrayList <String>();
        ArrayList <String> vettore_tipo_lesione = new ArrayList <String>();
        ArrayList <String> vettore_deceduto = new ArrayList <String>();
        ArrayList <String> vettore_cintura_casco_utilizzato = new ArrayList <String>();
        ArrayList <String> vettore_airbag = new ArrayList <String>();
        ArrayList <String> vettore_deceduto_dopo = new ArrayList <String>();
        
        try {
        	
        	br.readLine(); //leggo l'header del file .csv
        	
			while ((line = br.readLine()) != null) {
				String[] stringa = line.split(cvsSplitBy);
			
				numero_record_persone_in_auto++;
				vettore_id_incidenti.add(Integer.parseInt(stringa[0]));
				vettore_id_veicoli.add(Integer.parseInt(stringa[1]));
				vettore_progressivo.add(Integer.parseInt(stringa[2]));
				vettore_tipo_persona.add(stringa[3]);
				vettore_anno_nascita.add(Integer.parseInt(stringa[4]));
				vettore_sesso.add(stringa[5]);
				vettore_tipo_lesione.add(stringa[6]);
				vettore_deceduto.add(stringa[7]);
				vettore_cintura_casco_utilizzato.add(stringa[8]);
				vettore_airbag.add(stringa[9]);
				vettore_deceduto_dopo.add(stringa[10]);
			}
		} catch (NumberFormatException e) {
			System.out.println("Problemi di NumberFormatException!");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("Problemi di IOException!");
			e.printStackTrace();
		}
        
        System.out.println("Il numero di persone (in auto) lette � " + numero_record_persone_in_auto); //non compreso l'header del file .csv   
        
        /*Scrittura su file .csv del risultato finale ottenuto tramite l'algoritmo*/
    	
    	FileWriter writer = aperturaFileOutputCSV();
    	
    	salvataggioSuFileCSV(writer, vettore_id_incidenti, vettore_id_veicoli, vettore_progressivo, vettore_tipo_persona, vettore_anno_nascita, vettore_sesso, vettore_tipo_lesione,
    			vettore_deceduto, vettore_cintura_casco_utilizzato, vettore_airbag, vettore_deceduto_dopo, numero_record_persone_in_auto);
    	
    	chiusuraFileOutputCSV(writer);
    }

	private static void chiusuraFileOutputCSV(FileWriter writer) {
		try {
	        writer.flush();
	        writer.close();
	    } catch (IOException e) {
	        System.out.println("Errore nel flush e/o nella chiusura del writer!");
	        e.printStackTrace();
	    }
	}
	
	private static FileWriter aperturaFileOutputCSV() {
		String csv_header = "idPersona;progressivo;tipoPersona;annoNascita;sesso;tipoLesione;deceduto;cinturaCascoUtilizzato;airbag;decedutoDopo;idProtocollo;idVeicolo";
		FileWriter writer = null;
		
		try {
			writer = new FileWriter("prova.csv");
			writer.append(csv_header.toString());
		} catch (Exception e) {
	        System.out.println("Errore nella creazione del file di output in formato .csv!");
	        e.printStackTrace();
		}	
		return writer;
		
	}
	
	private static void salvataggioSuFileCSV(FileWriter writer, ArrayList<Integer> vettore_id_incidenti, ArrayList<Integer> vettore_id_veicoli, ArrayList<Integer> vettore_progressivo,
			ArrayList<String> vettore_tipo_persona, ArrayList<Integer> vettore_anno_nascita, ArrayList<String> vettore_sesso, ArrayList<String> vettore_tipo_lesione,
			ArrayList<String> vettore_deceduto, ArrayList<String> vettore_cintura_casco_utilizzato, ArrayList<String> vettore_airbag, ArrayList<String> vettore_deceduto_dopo, int numero_record_pedoni) {
		
		int id_persona_incrementale = 1;
		
		try {
			for (int i = 0; i < numero_record_pedoni; i++) {
				writer.append("\n");
				writer.append(String.valueOf(id_persona_incrementale));
				writer.append(";");
				writer.append(String.valueOf(vettore_progressivo.get(i)));
				writer.append(";");
				writer.append(String.valueOf(vettore_tipo_persona.get(i)));
				writer.append(";");
				writer.append(String.valueOf(vettore_anno_nascita.get(i)));
				writer.append(";");
				writer.append(String.valueOf(vettore_sesso.get(i)));
				writer.append(";");
				writer.append(String.valueOf(vettore_tipo_lesione.get(i)));
				writer.append(";");
				writer.append(String.valueOf(vettore_deceduto.get(i)));
				writer.append(";");
				writer.append(String.valueOf(vettore_cintura_casco_utilizzato.get(i)));
				writer.append(";");
				writer.append(String.valueOf(vettore_airbag.get(i)));
				writer.append(";");
				writer.append(String.valueOf(vettore_deceduto_dopo.get(i)));
				writer.append(";");
				writer.append(String.valueOf(vettore_id_incidenti.get(i)));
				writer.append(";");
				writer.append(String.valueOf(vettore_id_veicoli.get(i)));
				
				id_persona_incrementale++;
			}
		} catch (Exception e) {
			System.out.println("Errore nella scrittura del file di output in formato .csv!");
			e.getMessage();
		}
		
		System.out.println("\n");
		System.out.println("Tabella PersoneInAuto correttamente creato in un file di output in formato .csv!");
	}
	
	}
