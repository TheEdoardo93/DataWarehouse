package parsingMeteo;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class CrawlerParsing_Meteo {

	public static void main (String [] args){
		String url = "http://www.ilmeteo.it/portale/archivio-meteo/Roma";

		ArrayList <Document> meteo = new ArrayList<>();
		ArrayList <String> indirizzi= new ArrayList<>();

		String [] mesi = new String[12];
		String [] anni = new String [7];

		mesi [0] = "/Gennaio";
		mesi [1] = "/Febbraio";
		mesi [2] = "/Marzo";
		mesi [3] = "/Aprile";
		mesi [4] = "/Maggio";
		mesi [5] = "/Giugno";
		mesi [6] = "/Luglio";
		mesi [7] = "/Agosto";
		mesi [8] = "/Settembre";
		mesi [9] = "/Ottobre";
		mesi [10] = "/Novembre";
		mesi [11] = "/Dicembre";

		anni[0] = "/2010";
		anni[1] = "/2011";
		anni[2] = "/2012";
		anni[3] = "/2013";
		anni[4] = "/2014";
		anni[5] = "/2015";
		anni[6] = "/2016";

		for (int i=0; i<anni.length;i++){
			for(int j=0; j<mesi.length;j++){
				indirizzi.add(url+anni[i]+mesi[j]);
			}
		}

		Document doc = null;
		for(int i=0; i<indirizzi.size();i++){
			try {
				File f = new File("PagineMeteo/"+indirizzi.get(i).replaceAll("/|:|\\?", "_")+".html");
				if(!f.exists() && !f.isDirectory()){
					doc = Jsoup.parse(Jsoup.connect(indirizzi.get(i)).userAgent("Chrome/57.0.2987.110").timeout(30000).get().toString());
					System.out.println("Documento numero "+i+" aggiunto");
					salvaFile(doc, indirizzi.get(i));
					meteo.add(doc);
				}

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				TimeUnit.SECONDS.sleep(5);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		System.out.println(meteo.size());

	}

	private static void salvaFile(Document doc, String nomeFileCompleto) {
		FileWriter fileWr = null;

		String documento = doc.toString();
		try {
			fileWr = new FileWriter("PagineMeteo/"+nomeFileCompleto.replaceAll("/|:|\\?", "_")+".html", true);
			fileWr.append(documento);
			fileWr.flush();
			fileWr.close();	
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


	}

}
