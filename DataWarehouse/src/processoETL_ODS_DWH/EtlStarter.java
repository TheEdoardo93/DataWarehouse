package processoETL_ODS_DWH;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class EtlStarter {

	public static void main(String[] args) {

		System.out.println("Inizio del processo di ETL (Extract, Transform, Load) per l'alimentazione delle dimensioni del DWH" + "\n");
		
		/*Aggiornamento delle dimensioni 'Data', 'Orario' e 'Meteo'.*/
		
		System.out.println("--- Aggiornamento delle dimensioni 'Data', 'Orario' e 'Meteo'---");
		
		UpdateDataOraMeteo udom = new UpdateDataOraMeteo();
		udom.startUpdate();
		
		System.out.println("--- Processo di ETL della dimensione 'Data' terminato.");
		System.out.println("--- Processo di ETL della dimensione 'Meteo' terminato."); 
		System.out.println("--- Processo di ETL della dimensione 'Orario' terminato.");
		
		/*Aggiornamento della dimensione 'Strade'.*/
		
		System.out.println("\n");
		System.out.println("--- Aggiornamento della dimensione 'Strade'---");


		UpdateStrade upStrade = new UpdateStrade();
		try {
			upStrade.startUpdate();
		} catch (SQLException e) {
			System.out.println("\t\tAttenzione! Errore nell'aggiornamento della dimensione 'Strade'");
			System.out.println(e.getMessage());
		}
		System.out.println("--- Processo di ETL della dimensione 'Strade' terminato.");
	
		
		System.out.println("\n--- Aggiornamento della dimensione Veicoli");

		UpdateVeicoli upVeicoli = new UpdateVeicoli();
		try {
			upVeicoli.startUpdate();
		} catch (SQLException e) {
			System.out.println("\t\tAttenzione! Errore nell'aggiornamento della dimensione strade");
			System.out.println(e.getMessage());
		}
		System.out.println("--- Processo di ETL della dimensione 'Veicoli' terminato.");


		System.out.println("\n--- Aggiornamento della Helper Table Strade");
		
		HelperTableStrade helpStrade = new HelperTableStrade();
		helpStrade.startUpdate();
		System.out.println("--- Processo di ETL della dimensione 'Helper Table Strade' terminato.");


		//caricare i veicoli nella dimensione

		System.out.println("\n--- Aggiornamento della Helper Table Veicoli");
		HelperTableVeicoli helpVeicoli = new HelperTableVeicoli();
		helpVeicoli.startUpdate();
		System.out.println("--- Processo di ETL della dimensione 'Helper Table Veicoli' terminato.");


		System.out.println("\n--- Aggiornamento della dimensione PosizioniGeografiche");
		UpdateLuoghi upLuoghi= new UpdateLuoghi();
		upLuoghi.startUpdate();
		System.out.println("--- Processo di ETL della dimensione 'PosizioniGeografiche' terminato.");


		System.out.println("\n--- Aggiornamento delle Junk Dimensions");
		JunkDimension junks = new JunkDimension();
		try {
			junks.startUpdate();
			System.out.println("--- Processo di ETL di aggiornamento delle dimensioni 'Junk' terminato.");
		} catch (SQLException | IOException | InterruptedException e) {
			System.out.println("\t\tAttenzione! Errore nell'aggiornamento delle junk dimensions");
			System.out.println(e.getMessage());
		}

		System.out.println("\n--- Aggiornamento della dimensione PersoneCoinvolte");
		UpdatePedoniPersoneInAuto ppia= new UpdatePedoniPersoneInAuto();
		ppia.startUpdate();
		System.out.println("--- Processo di ETL di aggiornamento della dimensione 'PersoneCoinvolte' terminato.");


		System.out.println("\n--- Aggiornamento della dimensione Helper Table Persone");
		HelperTablePersoneCoinvolte helperPersone= new HelperTablePersoneCoinvolte();
		helperPersone.startUpdate();
		System.out.println("--- Processodi ETL di aggiornamento della dimensione 'Helper Table Persone' terminato.");
	}

}
