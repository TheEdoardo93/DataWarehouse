package processoETL_ODS_DWH;

import java.io.FileWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class HelperTableVeicoli {

	public ArrayList<ArrayList<Integer>> helperTableCaricata = new ArrayList<ArrayList<Integer>>();
	public ArrayList<ArrayList<Integer>> dati_da_scrivere= new ArrayList<ArrayList<Integer>>();

	/*
	public static void main (String [] args){
		startUpdate();
	}
	*/
	
	private boolean scritturaSuDatabase() {
		Connection conn = null;
		PreparedStatement statement = null;
		String file = "'tmpout/Helper_incidenti_veicoli/HelperTable_Incidenti_Veicoli.csv'";
		
		try {
			String query_load = "LOAD DATA LOCAL INFILE " + file +
								 " INTO TABLE DWH.Helper_Incidenti_Veicoli FIELDS TERMINATED BY ';' ENCLOSED BY '\"'" + 
								 " LINES TERMINATED BY '\n' IGNORE 1 LINES (idHelper_Incidenti_Veicoli,Veicoli_idVeicolo);";
			conn = DriverManager.getConnection("jdbc:mysql://localhost/dwh?user=root&password=password&verifyServerCertificate=false&useSSL=true");
			statement = conn.prepareStatement(query_load);
			statement.execute();
			conn.close();
			return true;
		} catch (SQLException e) {
			e.getSQLState();
			e.getErrorCode();
			e.getMessage();
		}
		return false;
	}
	
	
	public void startUpdate() {

		caricaHelperIncidentiVeicoli();
		
		System.out.println("\t\tDimensone array della helper: " + helperTableCaricata.size());
		System.out.println("\t\tDimensone array dei dati da scrivere: " + dati_da_scrivere.size());
		
		Connection conn = null;
		PreparedStatement statement = null;

		String query = "SELECT DWH.Veicoli.idVeicolo as DWid, ODS.Incidenti_ODS.idLuogo " +
				"FROM DWH.Veicoli, ODS.Veicoli_ODS, ODS.Incidenti_ODS " +
				"WHERE DWH.Veicoli.veicolo=ODS.Veicoli_ODS.idVeicolo AND ODS.Incidenti_ODS.idIncidente=ODS.Veicoli_ODS.idProtocollo " +
				"AND ods.veicoli_ods.updateTime > (select coalesce(MAX(date(dwh.helper_incidenti_veicoli.updateTime)),0) from dwh.helper_incidenti_veicoli)"
				+ " order by idLuogo, DWid";
		
		/*String query = "SELECT DWH.Veicoli.idVeicolo as DWid, ODS.Incidenti_ODS.idLuogo " +
				"FROM DWH.Veicoli, ODS.Veicoli_ODS, ODS.Incidenti_ODS " +
				"WHERE DWH.Veicoli.veicolo=ODS.Veicoli_ODS.idVeicolo AND ODS.Incidenti_ODS.idIncidente=ODS.Veicoli_ODS.idProtocollo " +
				"AND ods.veicoli_ods.updateTime > '2009-01-01 00:00:00'"+
				" order by idLuogo, DWid";*/

		try {
			conn = DriverManager.getConnection("jdbc:mysql://localhost/ods?user=root&password=password&verifyServerCertificate=false&useSSL=true");
			statement = conn.prepareStatement(query);
			ResultSet rs = statement.executeQuery();

			ArrayList<Integer> idLuoghi = new ArrayList<Integer>();
			ArrayList<Integer> idVeicoli = new ArrayList<Integer>();

			while (rs.next()) {
				idLuoghi.add(rs.getInt("idLuogo"));
				idVeicoli.add(rs.getInt("DWid"));
			}

			System.out.println("Dimensione Helper Table: " + helperTableCaricata.size());	
			
			
			int id_prev = idLuoghi.get(0);
			int id_corr = 0;
			ArrayList<Integer> temp = new ArrayList<Integer>();

			for (int i = 0; i < idLuoghi.size(); i++) {
				id_corr = idLuoghi.get(i);
				if (id_corr == id_prev) {
					temp.add(idVeicoli.get(i));
				} else {
					controllaDuplicati(temp);
					temp.clear();
					id_prev = id_corr;
					temp.add(idVeicoli.get(i));
				}
			}

			controllaDuplicati(temp);
			conn.close();

			scriviSuCSV();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	private void controllaDuplicati(ArrayList<Integer> temp) {
		
		ArrayList<Integer> risultatocopia = new ArrayList<Integer>();
		
		for (int i = 0; i < helperTableCaricata.size(); i++) {
			risultatocopia = helperTableCaricata.get(i);
			for(int j = 0; j< risultatocopia.size() && j < temp.size(); j++){
				if(risultatocopia.contains(temp.get(j))){
					risultatocopia.remove(temp.get(j));
				}
			}
			if (risultatocopia.isEmpty())
				break;
		}
		if(helperTableCaricata.isEmpty() ||!risultatocopia.isEmpty()){
			dati_da_scrivere.add((ArrayList<Integer>) temp.clone());
		}
	}

	private void scriviSuCSV() {
		
		System.out.println("CSV: dimensione array da scrivere: "+ dati_da_scrivere.size());
		
		String csv_header = "idHelper_Incidenti_Veicoli;Veicoli_idVeicolo";
		FileWriter writer = null;

		try {
			writer = new FileWriter("tmpout/Helper_incidenti_veicoli/HelperTable_Incidenti_Veicoli.csv", true);
			writer.append(csv_header.toString());
			writer.append("\n");
			for (int i = 0; i < dati_da_scrivere.size(); i++) {
				for (int j = 0; j < dati_da_scrivere.get(i).size(); j++) {
					writer.append(i+1+";"+dati_da_scrivere.get(i).get(j)+"\n");
				}
			}
			writer.close();
			boolean risultato = scritturaSuDatabase();
			if (risultato == false) {
				System.out.println("Attenzione: problemi nella scrittura su database della dimensione 'HelperTableStrade'!");
			}
		} catch (Exception e) {
			System.out.println("\t\tErrore nella creazione del file di output in formato .csv!");
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	private void caricaHelperIncidentiVeicoli() {
		
		dati_da_scrivere.clear();
		
		Connection conn = null;
		PreparedStatement statement = null;

		String query = "SELECT * from DWH.Helper_Incidenti_Veicoli order by idHelper_Incidenti_Veicoli, Veicoli_idVeicolo";

		try {
			conn = DriverManager.getConnection("jdbc:mysql://localhost/dwh?user=root&password=password&verifyServerCertificate=false&useSSL=true");
			statement = conn.prepareStatement(query);
			ResultSet rs = statement.executeQuery();


			ArrayList<Integer> temp = new ArrayList<Integer>();
			int id_corr=0;
			int id_prev=0;
			if(rs.next()){
				id_prev = rs.getInt("idHelper_Incidenti_Veicoli");
				temp.add(rs.getInt("Veicoli_idVeicolo"));
			
				while (rs.next()) {
					id_corr= rs.getInt("idHelper_Incidenti_Veicoli");
					if(id_corr==id_prev){
						temp.add(rs.getInt("Veicoli_idVeicolo"));
					}
					else{
						helperTableCaricata.add((ArrayList<Integer>) temp.clone());
						temp.clear();
						temp.add(rs.getInt("Veicoli_idVeicolo"));
						id_prev = id_corr;
					}
				}
				helperTableCaricata.add((ArrayList<Integer>) temp.clone());
			}
			conn.close();
		}catch(SQLException e) {
			e.printStackTrace();
		}
	}

}
