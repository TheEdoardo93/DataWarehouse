package processoETL_ODS_DWH;

import java.io.FileWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class UpdateLuoghi {

	/*
	public static void main (String [] args){
		startUpdate();
	}
	*/
	
	public void startUpdate() {

		ArrayList<ArrayList<String>> odsLuoghi = datiOdsLuoghi();

		if(!(odsLuoghi.isEmpty()))
			scriviSuCSV(odsLuoghi);

	}
	
	@SuppressWarnings("unchecked")
	private ArrayList<ArrayList<String>> datiOdsLuoghi() {

		ArrayList<ArrayList<String>> odsLuoghi = new ArrayList<ArrayList<String>>();
	
		ArrayList<String> temp = new ArrayList<String>();

		Connection conn = null;
		PreparedStatement statement = null;

		String queryDatiDaCaricare = "SELECT * from ODS.Luoghi_ODS where ODS.Luoghi_ODS.updateTime > "
				+ "(select coalesce(max(date(DWH.posizioniGeografiche.updateTime)),0) from DWH.posizioniGeografiche)";

		try {
			conn = DriverManager.getConnection("jdbc:mysql://localhost/ods?user=root&password=password&verifyServerCertificate=false&useSSL=true");
			statement = conn.prepareStatement(queryDatiDaCaricare);
			ResultSet rs = statement.executeQuery();

			while (rs.next()){
				temp.add(rs.getString("latitudine"));
				temp.add(rs.getString("longitudine"));
				temp.add(rs.getString("chilometrica"));
				temp.add(rs.getString("daSpecificare"));
				temp.add(rs.getString("particolaritāStrade"));
				temp.add(rs.getString("localizzazione"));
				temp.add(rs.getString("categoriaStrada"));
				temp.add(rs.getString("tipoStrada"));
				temp.add(rs.getString("fondoStradale"));
				temp.add(rs.getString("pavimentazione"));
				temp.add(rs.getString("segnaletica"));

				odsLuoghi.add((ArrayList<String>) temp.clone());
				temp.clear();
			}
			rs.close();
			
			
			conn.close();
		}catch (SQLException e){

		}
		return odsLuoghi;

	}

	private void scriviSuCSV(ArrayList<ArrayList<String>> odsLuoghi) {

		System.out.println("CSV: dimensione array da scrivere: "+ odsLuoghi.size());

		String csv_header = "latitudine;longitudine;chilometrica;daSpecificare;particolaritāStrade;localizzazione;categoriaStrada;tipoStrada;"
				+ "fondoStradale;pavimentazione;segnaletica";
		FileWriter writer = null;

		try {
			writer = new FileWriter("tmpout/Luoghi/LuoghiDWH.csv", true);
			writer.append(csv_header.toString());
			writer.append("\n");
			for (int i = 0; i < odsLuoghi.size(); i++) {
				for (int j = 0; j < odsLuoghi.get(i).size()-1; j++) {
					writer.append(odsLuoghi.get(i).get(j)+";");
				}
				writer.append(odsLuoghi.get(i).get(odsLuoghi.get(i).size()-1) + "\n");
			}
			writer.close();
			scritturaSuDatabase();
		} catch (Exception e) {
			System.out.println("\t\tErrore nella creazione del file di output in formato .csv!");
			e.printStackTrace();
		}
	}

	private boolean scritturaSuDatabase() {
		Connection conn = null;
		PreparedStatement statement = null;
		String file = "'tmpout/Luoghi/LuoghiDWH.csv'";

		try {
			String query_load = "LOAD DATA LOCAL INFILE " + file +
					" INTO TABLE DWH.PosizioniGeografiche FIELDS TERMINATED BY ';' ENCLOSED BY '\"'" + 
					" LINES TERMINATED BY '\n' IGNORE 1 LINES "
					+ "(latitudine,longitudine,chilometrica, daSpecificare, particolaritaStrade, "
					+ "localizzazione, categoriaStrada, tipoStrada,fondoStradale, pavimentazione, segnaletica);";
			
			conn = DriverManager.getConnection("jdbc:mysql://localhost/dwh?user=root&password=password&verifyServerCertificate=false&useSSL=true");
			statement = conn.prepareStatement(query_load);
			statement.execute();
			conn.close();
			return true;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return false;
	}
	

}

	/*
	private void aggiornaDimensione(ArrayList<ArrayList<String>> odsLuoghi) {
		Connection conn = null;
		PreparedStatement statement = null;

		String query = "INSERT IGNORE INTO DWH.PosizioniGeografiche "
				+ "(idPosizioneGeografica,latitudine,longitudine,chilometrica, daSpecificare, particolaritāStrade, localizzazione, categoriaStrada, tipoStrada,"
				+ " fondoStradale, pavimentazione, segnaletica, insertTime, updateTime)"
				+ " values(?,?,?,?,?,?,?,?,?,?,?,?, NOW(), NOW())";

		try {
			conn = DriverManager.getConnection("jdbc:mysql://localhost/dwh?user=root&password=password&verifyServerCertificate=false&useSSL=true");
			statement = conn.prepareStatement(query);

			for(int i=0; i<odsLuoghi.get(0).size(); i++){
				statement.setInt(1, Integer.parseInt(odsLuoghi.get(0).get(i)));
				statement.setString(2, odsLuoghi.get(1).get(i));
				statement.setString(3, odsLuoghi.get(2).get(i));
				statement.setString(4, odsLuoghi.get(3).get(i));
				statement.setString(5, odsLuoghi.get(4).get(i));
				statement.setString(6, odsLuoghi.get(5).get(i));
				statement.setString(7, odsLuoghi.get(6).get(i));
				statement.setString(8, odsLuoghi.get(7).get(i));
				statement.setString(9, odsLuoghi.get(8).get(i));
				statement.setString(10, odsLuoghi.get(9).get(i));
				statement.setString(11, odsLuoghi.get(10).get(i));
				statement.setString(12, odsLuoghi.get(11).get(i));

				statement.execute();
			}

			conn.close();


		}catch(SQLException e){

		}

	}
*/