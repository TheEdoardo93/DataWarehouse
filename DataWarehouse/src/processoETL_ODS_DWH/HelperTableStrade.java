package processoETL_ODS_DWH;

import java.io.FileWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class HelperTableStrade {

	public ArrayList<ArrayList<Integer>> helperTableCaricata = new ArrayList<ArrayList<Integer>>();
	
	/*
	public static void main(String[] args) {
		startUpdate();
	}
	 */
	@SuppressWarnings("unchecked")
	public void startUpdate() {

		int max = caricaHelperIncidentiStrade();

		System.out.println("\t\tDimensone array della helper: " + helperTableCaricata.size());

		Connection conn = null;
		PreparedStatement statement = null;

		String query = "SELECT DISTINCT(ODS.Strade_ODS.strada) as strada, DWH.Strade.idStrada as DWid, ODS.Strade_ODS.idStrada, ODS.Luoghi_ODS.idLuogo as idLuogo " +
				"FROM ODS.Strade_ODS, ODS.Luoghi_ODS, ODS.Luoghi_Strade_ODS, DWH.strade " +
				"WHERE ODS.Luoghi_Strade_ODS.idLuogo = ODS.Luoghi_ODS.idLuogo AND ODS.Strade_ODS.idStrada=ODS.Luoghi_Strade_ODS.idStrada " +
				"AND DWH.Strade.strada = ODS.Strade_ODS.strada ORDER BY ODS.Luoghi_ODS.idLuogo, DWH.Strade.idStrada";

		try {
			conn = DriverManager.getConnection("jdbc:mysql://localhost/ods?user=root&password=password&verifyServerCertificate=false&useSSL=true");
			statement = conn.prepareStatement(query);
			ResultSet rs = statement.executeQuery();

			ArrayList<ArrayList<Integer>> strade = new ArrayList<ArrayList<Integer>>();
			//ArrayList<Integer> idVeicoli = new ArrayList<Integer>();

			ArrayList<Integer> temp = new ArrayList<Integer>();
			int id_corr=0;
			int id_prev=0;

			if(rs.next()){
				id_prev = rs.getInt("idLuogo");
				temp.add(rs.getInt("DWid"));

				while (rs.next()) {
					id_corr= rs.getInt("idLuogo");
					if(id_corr==id_prev){
						temp.add(rs.getInt("DWid"));
					}
					else{
						strade.add((ArrayList<Integer>) temp.clone());
						temp.clear();
						temp.add(rs.getInt("DWid"));
						id_prev = id_corr;
					}
				}
				strade.add((ArrayList<Integer>) temp.clone());
			}

			ArrayList<ArrayList<Integer>> dati_da_scrivere = controllaDuplicati(strade,max);
			conn.close();

			scriviSuCSV(dati_da_scrivere);
			//scritturaInDatabase();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	private ArrayList<ArrayList<Integer>> controllaDuplicati(ArrayList<ArrayList<Integer>> strade, int max) {
		if(helperTableCaricata.size()==0){
			helperTableCaricata.add(strade.get(0));
			for (ArrayList<Integer> s : strade) {
				boolean presente = false;
				for (ArrayList<Integer> h : helperTableCaricata) {
					if (s.equals(h)) {
						presente = true;
						break;
					}
				}
				if(!presente)
					helperTableCaricata.add((ArrayList<Integer>) s.clone());
			}
			return helperTableCaricata;
		}
		else{
			for (ArrayList<Integer> s : strade) {
				boolean presente = false;
				for (ArrayList<Integer> h : helperTableCaricata) {
					if (s.equals(h)) {
						presente = true;
						break;
					}
				}
				if(!presente)
					helperTableCaricata.add((ArrayList<Integer>) s.clone());
			}
		}
		return helperTableCaricata;
	}

	private void scriviSuCSV(ArrayList<ArrayList<Integer>> dati_da_scrivere) {

		System.out.println("\t\tCSV: dimensione array da scrivere: "+ dati_da_scrivere.size());

		String csv_header = "idHelper_Incidenti_Strade;Strade_idStrada";
		FileWriter writer = null;

		try {
			writer = new FileWriter("tmpout/HelperTable_Strade/HelperTable_Incidenti_Strade.csv", true);
			writer.append(csv_header.toString());
			writer.append("\n");
			for (int i = 0; i < dati_da_scrivere.size(); i++) {
				for (int j = 0; j < dati_da_scrivere.get(i).size(); j++) {
					writer.append(i+1+";"+dati_da_scrivere.get(i).get(j)+"\n");
				}
			}
			writer.close();
			boolean risultato = scritturaSuDatabase();
			if (risultato == false) {
				System.out.println("Attenzione: problemi nella scrittura su database della dimensione 'HelperTableStrade'!");
			}
		} catch (Exception e) {
			System.out.println("\t\tErrore nella creazione del file di output in formato .csv!");
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	private int caricaHelperIncidentiStrade() {

		Connection conn = null;
		PreparedStatement statement = null;

		String query = "SELECT * from DWH.Helper_Incidenti_Strade order by idHelper_Incidenti_Strade, Strade_idStrada";
		String query2 = "SELECT MAX(DWH.Helper_Incidenti_Strade.idHelper_Incidenti_Strade) as m FROM DWH.Helper_Incidenti_Strade";

		try {
			conn = DriverManager.getConnection("jdbc:mysql://localhost/dwh?user=root&password=password&verifyServerCertificate=false&useSSL=true");
			statement = conn.prepareStatement(query2);
			ResultSet rs = statement.executeQuery();

			rs.next();
			int max = rs.getInt("m");

			statement = conn.prepareStatement(query);
			rs = statement.executeQuery();

			ArrayList<Integer> temp = new ArrayList<Integer>();
			int id_corr=0;
			int id_prev=0;
			if(rs.next()){
				id_prev = rs.getInt("idHelper_Incidenti_Strade");
				temp.add(rs.getInt("Strade_idStrada"));

				while (rs.next()) {
					id_corr= rs.getInt("idHelper_Incidenti_Strade");
					if(id_corr==id_prev){
						temp.add(rs.getInt("Strade_idStrada"));
					}
					else{
						helperTableCaricata.add((ArrayList<Integer>) temp.clone());
						temp.clear();
						temp.add(rs.getInt("Strade_idStrada"));
						id_prev = id_corr;
					}
				}
				helperTableCaricata.add((ArrayList<Integer>) temp.clone());
			}
			conn.close();
			return max;
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	private boolean scritturaSuDatabase() {
		Connection conn = null;
		PreparedStatement statement = null;
		String file = "'tmpout/HelperTable_Strade/HelperTable_Incidenti_Strade.csv'";
		
		try {
			String query_load = "LOAD DATA LOCAL INFILE " + file +
								 " INTO TABLE DWH.Helper_Incidenti_Strade FIELDS TERMINATED BY ';' ENCLOSED BY '\"'" + 
								 " LINES TERMINATED BY '\n' IGNORE 1 LINES (idHelper_Incidenti_Strade,Strade_idStrada);";
			conn = DriverManager.getConnection("jdbc:mysql://localhost/dwh?user=root&password=password&verifyServerCertificate=false&useSSL=true");
			statement = conn.prepareStatement(query_load);
			statement.execute();
			conn.close();
			return true;
		} catch (SQLException e) {
			e.getSQLState();
			e.getErrorCode();
			e.getMessage();
		}
		return false;
	}

}
