package processoETL_ODS_DWH;

import java.io.FileWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class UpdateStrade {

	/*
	 public static void main(String[] args) {
	 
		try {
			startUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	*/

	public void startUpdate() throws SQLException {

		//caricamento dei dati dal DWH
		ArrayList<String> dwhStrade= caricaDimensioneStrade();

		//caricamento dei dati dall'ODS
		ArrayList<String> odsStrade = datiOdsStrade();
		ArrayList<String> dati_da_scrivere = new ArrayList<String>();

		if (!(odsStrade.isEmpty())) {
			if(dwhStrade.isEmpty())
			{
				scriviSuCSV(odsStrade);
				//scritturaInDatabase(odsStrade);
			}
			else{
				boolean trovato = false;
				for(int i =0; i<odsStrade.size(); i++){
					for(int j=0; i<dwhStrade.size(); j++){
						if(!(dwhStrade.get(j).equalsIgnoreCase(odsStrade.get(i))))
							trovato = false;

						else{
							trovato = true;
							break;
						}
					}
					if(!trovato)
						dati_da_scrivere.add(odsStrade.get(i));
				}
				scriviSuCSV(dati_da_scrivere);
				//scritturaInDatabase(dati_da_scrivere);
			}

		}
	}

	private ArrayList<String> datiOdsStrade() {

		ArrayList<String> odsStrade = new ArrayList<String>();

		Connection conn = null;
		PreparedStatement statement = null;

		String queryDatiDaCaricare = "SELECT DISTINCT(strada) from ODS.strade_ODS WHERE ODS.strade_ODS.updateTime > (SELECT coalesce(MAX(date(DWH.Strade.updateTime)),0) FROM DWH.Strade)";

		try {
			conn = DriverManager.getConnection("jdbc:mysql://localhost/ODS?user=root&password=password&verifyServerCertificate=false&useSSL=true");
			statement = conn.prepareStatement(queryDatiDaCaricare);
			ResultSet rs = statement.executeQuery();

			while (rs.next())
				odsStrade.add(rs.getString("strada"));

			conn.close();
		}catch (SQLException e){
			System.out.println(e.getMessage());
		}

		System.out.println("\t\tDimensione array strade ODS: " + odsStrade.size());
		return odsStrade;


	}

	private ArrayList<String> caricaDimensioneStrade() {
		Connection conn = null;
		PreparedStatement statement = null;

		ArrayList<String> dwhStrade= new ArrayList<String>();
		String queryDimensione = "SELECT strada from DWH.Strade";

		try {
			conn = DriverManager.getConnection("jdbc:mysql://localhost/DWH?user=root&password=password&verifyServerCertificate=false&useSSL=true");
			statement = conn.prepareStatement(queryDimensione);
			ResultSet rs = statement.executeQuery();


			while (rs.next())
				dwhStrade.add(rs.getString("strada"));


		}catch (SQLException e) {
			e.printStackTrace();
		}

		System.out.println("\t\tDimensione array strade DWH: " + dwhStrade.size());

		return dwhStrade;
	}

	private void scriviSuCSV(ArrayList<String> dati_da_scrivere) {

		System.out.println("CSV: dimensione array da scrivere: "+ dati_da_scrivere.size());

		String csv_header = "strada";
		FileWriter writer = null;

		try {
			writer = new FileWriter("tmpout/Strade/dwhStrade.csv", true);
			writer.append(csv_header.toString());
			writer.append("\n");

			for (int i = 0; i < dati_da_scrivere.size(); i++) 
				writer.append(i+1+";"+dati_da_scrivere.get(i)+"\n");

			writer.close();
			boolean risultato = scritturaSuDatabase();
			if (risultato == false) {
				System.out.println("Attenzione: problemi nella scrittura su database della dimensione 'Strade'!");
			}
		} catch (Exception e) {
			System.out.println("\t\tErrore nella creazione del file di output in formato .csv!");
			e.printStackTrace();
		}
	}

	private boolean scritturaSuDatabase() {
		Connection conn = null;
		PreparedStatement statement = null;
		String file = "'tmpout/Strade/dwhStrade.csv'";

		try {
			String query_load = "LOAD DATA LOCAL INFILE " + file +
					" INTO TABLE Strade FIELDS TERMINATED BY ';' ENCLOSED BY '\"'" + 
					" LINES TERMINATED BY '\n' IGNORE 1 LINES (idStrada,strada);";
			conn = DriverManager.getConnection("jdbc:mysql://localhost/dwh?user=root&password=password&verifyServerCertificate=false&useSSL=true");
			statement = conn.prepareStatement(query_load);
			statement.execute();
			conn.close();
			return true;
		} catch (SQLException e) {
			e.getSQLState();
			e.getErrorCode();
			e.getMessage();
		}
		return false;
	}

}
