package processoETL_ODS_DWH;

import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;

public class JunkDimension {
	
	/*
	public static void main(String[] args) throws SQLException, IOException, InterruptedException {
		startUpdate();
	}
	*/

	public void startUpdate() throws SQLException, IOException, InterruptedException {

		String connection_ODS = "jdbc:mysql://localhost/ODS?user=root&password=password&verifyServerCertificate=false&useSSL=true";
		String from_ods= "ODS.Incidenti_ODS";
		String connection_DWH = "jdbc:mysql://localhost/DWH?user=root&password=password&verifyServerCertificate=false&useSSL=true";
		String from_DWH1= "DWH.InformazioniIncidenti";
		String from_DWH2= "DWH.CondizioniIncidenti";

		ArrayList<ArrayList<String>> ods = caricaDati(from_ods, from_ods, connection_ODS);
		ArrayList<ArrayList<String>> dwh = caricaDati(from_DWH1, from_DWH2,connection_DWH);
		
		if(checkDWH(dwh) || dwh.size()< ods.size())
			creaJunk(ods);
		else{
			boolean valori_identici = true;
			for(int i=0; i<ods.size();i++){
				for(int j=0; j<ods.get(i).size(); j++){
					if(!(ods.get(i).get(j)==null || ods.get(i).get(j).equalsIgnoreCase("null"))){
						if(!(ods.get(i).get(j).equalsIgnoreCase(dwh.get(i).get(j)))){
							valori_identici = false;
							break;
						}
					}
				}
			}
			if(!valori_identici){
				creaJunk(ods);
			}
			else
				System.out.println("\t\tNessun dato aggiunto");
		}
		//System.out.println("Processo Terminato");
	}

	private boolean checkDWH(ArrayList<ArrayList<String>> dwh) {
		for(int i =0; i< dwh.size(); i++)
			if(dwh.get(i).isEmpty()){
				return true;
			}
		return false;
	}

	private void creaJunk(ArrayList<ArrayList<String>> ods) throws IOException {
		
		ArrayList<String> natInc = ods.get(0);
		ArrayList<String> groups = ods.get(1);
		ArrayList<String> traff = ods.get(2);
		ArrayList<String> ill = ods.get(3);
		ArrayList<String> vis = ods.get(4);
		ArrayList<String> condAtm = ods.get(5);

		ArrayList<String> junk1 = new ArrayList<>();
		ArrayList<String> junk2 = new ArrayList<>();


		for(int i=0; i<natInc.size(); i++){
			for(int j=0; j<groups.size(); j++){
				junk1.add(natInc.get(i)+";"+groups.get(j));
			}
		}

		for(int i=0; i<ill.size(); i++){
			for(int j=0; j<traff.size(); j++){
				for(int k=0; k<vis.size(); k++){
					for(int w=0; w<condAtm.size(); w++){
						junk2.add(ill.get(i) +";"+ traff.get(j) +";"+ vis.get(k)+";"+condAtm.get(w));
					}
				}
			}
		}

		FileWriter filejunk1 = new FileWriter("tmpout/Junk/junk1.csv");
		FileWriter filejunk2 = new FileWriter("tmpout/Junk/junk2.csv");

		filejunk1.append("naturaIncidente;gruppo\n");
		for(int i=0; i<junk1.size();i++)
			filejunk1.append(junk1.get(i)+"\n");

		filejunk1.close();

		System.out.println("\t\tJunk InformazioniIncidenti creata");

		filejunk2.append("illuminazione;traffico;visibilita;condizioneAtmosferica\n");
		for(int i=0; i<junk2.size();i++)
			filejunk2.append(junk2.get(i)+"\n");

		filejunk2.close();

		System.out.println("\t\tJunk CondizioniIncidenti creata");
		
		caricaSuDwh();
	}

	private void caricaSuDwh(){
		String loadJunk1 = "LOAD DATA LOCAL INFILE 'tmpout/Junk/junk1.csv' "+
				"INTO TABLE DWH.InformazioniIncidenti " +
				"FIELDS TERMINATED BY ';' "+
				"ENCLOSED BY '\"' "+ 
				"LINES TERMINATED BY '\n' " +
				"IGNORE 1 LINES "+
				"(naturaIncidente, gruppo);";
		
		String loadJunk2 = "LOAD DATA LOCAL INFILE 'tmpout/Junk/junk2.csv' "+
				"INTO TABLE DWH.CondizioniIncidenti " +
				"FIELDS TERMINATED BY ';' "+
				"ENCLOSED BY '\"' "+ 
				"LINES TERMINATED BY '\n' " +
				"IGNORE 1 LINES "+
				"(illuminazione, traffico, visibilita, condizioneAtmosferica);";
		
		String update1 = "UPDATE CondizioniIncidenti SET traffico = IF (traffico = 'null', ?, traffico);";
		String update2 = "UPDATE CondizioniIncidenti SET visibilita = IF (visibilita = 'null', ?, visibilita);";
		String update3 = "UPDATE CondizioniIncidenti SET condizioneAtmosferica = IF (condizioneAtmosferica = 'null', ?, condizioneAtmosferica);";
		String update4 = "UPDATE CondizioniIncidenti SET illuminazione = IF (illuminazione = 'null', ?, illuminazione);";
		
		Connection conn;
		
		try {
			conn = DriverManager.getConnection("jdbc:mysql://localhost/DWH?user=root&password=password&verifyServerCertificate=false&useSSL=true");
			
			PreparedStatement j1 = conn.prepareStatement(loadJunk1);
			PreparedStatement j2 = conn.prepareStatement(loadJunk2);
			PreparedStatement uj1 = conn.prepareStatement(update1);
			PreparedStatement uj2 = conn.prepareStatement(update2);
			PreparedStatement uj3 = conn.prepareStatement(update3);
			PreparedStatement uj4 = conn.prepareStatement(update4);
			
			uj1.setNull(1, Types.VARCHAR);
			uj2.setNull(1, Types.VARCHAR);
			uj3.setNull(1, Types.VARCHAR);
			uj4.setNull(1, Types.VARCHAR);
			
			j1.execute();
			j2.execute();
			uj1.executeUpdate();
			uj2.executeUpdate();
			uj3.executeUpdate();
			uj4.executeUpdate();
			
			conn.close();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	@SuppressWarnings("unchecked")
	private ArrayList<ArrayList<String>> caricaDati(String from1, String from2, String connection){
		
		Connection conn;

		ArrayList<ArrayList<String>> risultati = new ArrayList<ArrayList<String>>();
		try {
			ArrayList<String> temp = new ArrayList<>();
			conn = DriverManager.getConnection(connection);

			String query_naturaIncindente = "SELECT distinct(naturaIncidente) FROM " + from1 + " order by naturaIncidente ASC";
			String query_gruppo = "SELECT distinct(gruppo) FROM " + from1+ " order by gruppo ASC";
			String query_traffico = "SELECT distinct(traffico) FROM " + from2+ " order by traffico ASC";
			String query_illuminazione = "SELECT distinct(illuminazione) FROM " + from2+ " order by illuminazione ASC";
			String query_vibilitÓ = "SELECT distinct(visibilita) FROM " + from2+ " order by visibilita ASC";
			String query_condizioneAtmosferica = "SELECT distinct(condizioneAtmosferica) FROM " + from2+ " order by condizioneAtmosferica ASC";

			PreparedStatement preparedStmtnaturaIncidente = conn.prepareStatement(query_naturaIncindente);
			PreparedStatement preparedStmtGruppo = conn.prepareStatement(query_gruppo);
			PreparedStatement preparedStmtTraffico = conn.prepareStatement(query_traffico);
			PreparedStatement preparedStmtIlluminazione = conn.prepareStatement(query_illuminazione);
			PreparedStatement preparedStmtVibilitÓ = conn.prepareStatement(query_vibilitÓ);
			PreparedStatement preparedStmtCondizioneAtmosferica = conn.prepareStatement(query_condizioneAtmosferica);

			ResultSet rs1 = preparedStmtnaturaIncidente.executeQuery();
			ResultSet rs2 = preparedStmtGruppo.executeQuery();
			ResultSet rs3 = preparedStmtTraffico.executeQuery();
			ResultSet rs4 = preparedStmtIlluminazione.executeQuery();
			ResultSet rs5 = preparedStmtVibilitÓ.executeQuery();
			ResultSet rs6 = preparedStmtCondizioneAtmosferica.executeQuery();

			
			while(rs1.next())
				temp.add(rs1.getString("naturaIncidente"));
			risultati.add((ArrayList<String>) temp.clone());
			
			temp.clear();
			rs1.close();

			while(rs2.next())
				temp.add(((Integer)rs2.getInt("gruppo")).toString());
			
			risultati.add((ArrayList<String>) temp.clone());
			temp.clear();
			rs2.close();

			while(rs3.next())
				temp.add(rs3.getString("traffico"));
			
			risultati.add((ArrayList<String>) temp.clone());
			temp.clear();
			rs3.close();

			while(rs4.next())
				temp.add(rs4.getString("illuminazione"));
			risultati.add((ArrayList<String>) temp.clone());
			temp.clear();
			rs4.close();

			while(rs5.next())
				temp.add(rs5.getString("visibilita"));
			risultati.add((ArrayList<String>) temp.clone());
			temp.clear();
			rs5.close();

			while(rs6.next())
				temp.add(rs6.getString("condizioneAtmosferica"));
			risultati.add((ArrayList<String>) temp.clone());
			temp.clear();
			rs6.close();

			conn.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return risultati;
	}

}
