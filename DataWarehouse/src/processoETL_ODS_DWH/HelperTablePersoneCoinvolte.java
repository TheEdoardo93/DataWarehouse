package processoETL_ODS_DWH;

import java.io.FileWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class HelperTablePersoneCoinvolte {

	/*
	public static void main(String[] args) {
		startUpdate();
	}
	*/
	@SuppressWarnings("unchecked")
	public void startUpdate() {

		Connection conn = null;

		try {
			conn = DriverManager.getConnection("jdbc:mysql://localhost/ODS?user=root&password=password&verifyServerCertificate=false&useSSL=true");
			/*String query = "SELECT ODS.PersoneInAuto_ODS.idProtocollo, ODS.PersoneInAuto_ODS.idPersonaInAuto, ODS.Pedoni_ODS.idPedone "
					+ "FROM ODS.PersoneInAuto_ODS LEFT JOIN ODS.Pedoni_ODS on ODS.Pedoni_ODS.idProtocollo = ODS.PersoneInAuto_ODS.idProtocollo"
					+ " WHERE ODS.PersoneInAuto_ODS.updateTime > (select coalesce(max(date(DWH.PersoneCoinvolte.updateTime)),0) from DWH.PersoneCoinvolte)"
					+ " ORDER BY ODS.PersoneInAuto_ODS.idProtocollo";*/

			String query_popolamento_iniziale = "SELECT ODS.PersoneInAuto_ODS.idProtocollo, ODS.PersoneInAuto_ODS.idPersonaInAuto, ODS.Pedoni_ODS.idPedone"
					+ " FROM ODS.PersoneInAuto_ODS LEFT JOIN ODS.Pedoni_ODS on ODS.Pedoni_ODS.idProtocollo = ODS.PersoneInAuto_ODS.idProtocollo"
					+ " WHERE ODS.PersoneInAuto_ODS.updateTime > '2009-01-01 00:00:00'"
					+ " ORDER BY ODS.PersoneInAuto_ODS.idProtocollo";

			PreparedStatement p = conn.prepareStatement(query_popolamento_iniziale);

			ResultSet rs = p.executeQuery();

			ArrayList<ArrayList<Integer>> helper_dwh = new ArrayList<ArrayList<Integer>>();

			ArrayList<Integer> protocollo = new ArrayList<Integer>();
			ArrayList<Integer> pers = new ArrayList<Integer>();
			ArrayList<Integer> pedone = new ArrayList<Integer>();

			while(rs.next()){
				protocollo.add(rs.getInt("idProtocollo"));
				pers.add(rs.getInt("idPersonaInAuto"));
				pedone.add(rs.getInt("idPedone"));
			}

			rs = conn.prepareStatement("select count(*) as c from ODS.PersoneInAuto_ODS").executeQuery();
			rs.next();
			int numeroPersInAuto = rs.getInt("c");

			conn.close();

			int inc_prev = protocollo.get(0);
			int inc_curr = 0;

			ArrayList<Integer> temp = new ArrayList<Integer>();

			for(int i = 0; i<protocollo.size(); i++){
				inc_curr = protocollo.get(i);
				if(inc_curr == inc_prev){
					temp.add(pers.get(i));
					if(pedone.get(i) != 0)
						temp.add(pedone.get(i) + numeroPersInAuto);
				}
				else{
					helper_dwh.add((ArrayList<Integer>) temp.clone());
					temp.clear();
					inc_prev = inc_curr;
					temp.add(pers.get(i));
					if(pedone.get(i) != 0)
						temp.add(pedone.get(i) + numeroPersInAuto);
				}
			}
			
			scriviSuCSV(helper_dwh);

		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}	
	}

	private void scriviSuCSV(ArrayList<ArrayList<Integer>> dati_da_scrivere) {

		System.out.println("CSV: dimensione array da scrivere: "+ dati_da_scrivere.size());

		String csv_header = "idGruppo,idPersona";
		FileWriter writer = null;

		try {
			writer = new FileWriter("tmpout/Helper_incidenti_persone/HelperdwhPersonePedoni.csv", true);
			writer.append(csv_header.toString());
			writer.append("\n");

			for (int i = 0; i < dati_da_scrivere.size(); i++) {
				for (int j = 0; j < dati_da_scrivere.get(i).size(); j++) {
					writer.append(i+1+";"+dati_da_scrivere.get(i).get(j)+"\n");
				}
			}

			writer.close();
			boolean risultato = scritturaSuDatabase();
			if (risultato == false) {
				System.out.println("Attenzione: problemi nella scrittura su database della dimensione 'HelperTablePersoneCoinvolte'!");
			}
		} catch (Exception e) {
			System.out.println("\t\tErrore nella creazione del file di output in formato .csv!");
			e.printStackTrace();
		}
	}

	private boolean scritturaSuDatabase() {
		Connection conn = null;
		PreparedStatement statement = null;
		String file = "'tmpout/Helper_incidenti_persone/HelperdwhPersonePedoni.csv'";

		try {
			String query_load = "LOAD DATA LOCAL INFILE " + file +
					" INTO TABLE Helper_Incidenti_PersoneCoinvolte FIELDS TERMINATED BY ';' ENCLOSED BY '\"'" + 
					" LINES TERMINATED BY '\n' IGNORE 1 LINES (idHelper_Incidenti_PersoneCoinvolte,PersoneCoinvolte_idPersona);";
			conn = DriverManager.getConnection("jdbc:mysql://localhost/dwh?user=root&password=password&verifyServerCertificate=false&useSSL=true");
			statement = conn.prepareStatement(query_load);
			statement.execute();
			conn.close();
			return true;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return false;
	}

}
