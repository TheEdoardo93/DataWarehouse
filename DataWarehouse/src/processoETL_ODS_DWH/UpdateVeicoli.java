package processoETL_ODS_DWH;

import java.io.FileWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class UpdateVeicoli {

	/*
	public static void main (String [] args){
		try {
			startUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	*/

	public void startUpdate() throws SQLException{

		String query_ods = "SELECT idVeicolo, progressivo, marca, modello, tipoVeicolo, statoVeicolo "
				+ "FROM ods.veicoli_ods WHERE ods.veicoli_ODS.updateTime > ? order by idVeicolo ASC";
		String query_dwh = "SELECT idVeicolo, veicolo FROM DWH.veicoli order by veicolo ASC";

		ArrayList <ArrayList<String>> veicoli_ods = caricaDati(query_ods);

		System.out.println(veicoli_ods.size());

		if(veicoli_ods.size()>0){
			ArrayList <ArrayList<String>> veicoli_dwh = caricaDati(query_dwh);
			System.out.println(veicoli_dwh.size());
			if(veicoli_dwh.size()>0){
				ArrayList <ArrayList<String>> dati_da_scrivere = controllaDuplicati(veicoli_ods, veicoli_dwh);
				//scritturaInDatabase(dati_da_scrivere);
				scriviSuCsv(dati_da_scrivere);
			}
			//scritturaInDatabase(veicoli_ods)
			scriviSuCsv(veicoli_ods);
		}
	}

	@SuppressWarnings("unchecked")
	private ArrayList<ArrayList<String>> controllaDuplicati(ArrayList<ArrayList<String>> veicoli_ods,
			ArrayList<ArrayList<String>> veicoli_dwh) {

		ArrayList<ArrayList<String>> veicoli_output = new ArrayList<ArrayList<String>>();
		for(ArrayList<String> vei_ods : veicoli_ods){
			for(int i =0; i< veicoli_dwh.size();i++){
				if(!(veicoli_dwh.get(i).get(1).equals(vei_ods.get(0)))){
					veicoli_output.add((ArrayList<String>) vei_ods.clone());
				}
			}
		}

		return veicoli_ods;
	}

	private boolean scritturaSuDatabase() {
		Connection conn = null;
		PreparedStatement statement = null;
		String file = "'tmpout/Veicoli/Veicolidwh.csv'";

		try {
			String query_load = "LOAD DATA LOCAL INFILE " + file +
					" INTO TABLE DWH.Veicoli FIELDS TERMINATED BY ';' ENCLOSED BY '\"'" + 
					" LINES TERMINATED BY '\n' IGNORE 1 LINES (veicolo,progressivo,marca,modello,tipoVeicolo,statoVeicolo);";
			conn = DriverManager.getConnection("jdbc:mysql://localhost/dwh?user=root&password=password&verifyServerCertificate=false&useSSL=true");
			statement = conn.prepareStatement(query_load);
			statement.execute();
			conn.close();
			return true;
		} catch (SQLException e) {
			e.getSQLState();
			e.getErrorCode();
			System.out.println(e.getMessage());
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	private ArrayList<ArrayList<String>> caricaDati(String query) {

		ArrayList<String> tmp = new ArrayList <String>();
		ArrayList<ArrayList<String>> risultato = new ArrayList<ArrayList<String>>();

		Connection conn = null;
		PreparedStatement statement1 = null;
		PreparedStatement statement = null;

		String ods = "jdbc:mysql://localhost/ods?user=root&password=password&verifyServerCertificate=false&useSSL=true";
		String dwh = "jdbc:mysql://localhost/dwh?user=root&password=password&verifyServerCertificate=false&useSSL=true";


		try {
			if(query.indexOf("ods")!=-1){
				conn = DriverManager.getConnection(ods);
				statement1 = conn.prepareStatement("SELECT max(updateTime) as ud from DWH.Veicoli");
				ResultSet rs1 = statement1.executeQuery();

				rs1.next();
				String data = rs1.getString("ud");

				statement = conn.prepareStatement(query);
				if(data == null)
					statement.setString(1, "1900-01-01");
				else
					statement.setString(1, data);

				ResultSet rs= statement.executeQuery();

				while(rs.next()){
					tmp.add(rs.getString("idVeicolo"));
					tmp.add(rs.getString("progressivo"));
					tmp.add(rs.getString("marca"));
					tmp.add(rs.getString("modello"));
					tmp.add(rs.getString("tipoVeicolo"));
					tmp.add(rs.getString("statoVeicolo"));
					risultato.add((ArrayList<String>) tmp.clone());
					tmp.clear();
				}

				conn.close();
				return risultato;
			} 
			else{
				conn = DriverManager.getConnection(dwh);
				statement = conn.prepareStatement(query);
				ResultSet rs = statement.executeQuery();

				while(rs.next()){
					tmp.add(rs.getString("idVeicolo"));
					tmp.add(rs.getString("veicolo"));
					risultato.add((ArrayList<String>) tmp.clone());
					tmp.clear();
				}

				conn.close();
				return risultato;
			}
		}catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	private void scriviSuCsv(ArrayList<ArrayList<String>> dati_da_scrivere){
		String csv_header = "veicolo;progressivo;marca;modello;tipoVeicolo;statoVeicolo";
		FileWriter writer = null;

		try {
			writer = new FileWriter("tmpout/Veicoli/Veicolidwh.csv", true);
			writer.append(csv_header.toString());
			writer.append("\n");

			for (int i = 0; i < dati_da_scrivere.size(); i++) {
				for(int j =0; j<dati_da_scrivere.get(i).size()-1; j++)					
					writer.append(dati_da_scrivere.get(i).get(j)+";");
				writer.append(dati_da_scrivere.get(i).get(dati_da_scrivere.get(i).size()-1));
				writer.append("\n");
			}
			writer.close();

			boolean risultato = scritturaSuDatabase();
			if (risultato == false) {
				System.out.println("Attenzione: problemi nella scrittura su database della dimensione 'Veicoli'!");
			}

		} catch (Exception e) {
			System.out.println("\t\tErrore nella creazione del file di output in formato .csv!");
			e.printStackTrace();
		}
	}

}
