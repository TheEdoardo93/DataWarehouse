package processoETL_ODS_DWH;

import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UpdateDataOraMeteo {

	/*
	public static void main(String[] args) {
		startUpdate();
	}
	*/

	public void startUpdate(){

		Connection conn = null;

		try {
			conn = DriverManager.getConnection("jdbc:mysql://localhost/dwh?user=root&password=password&verifyServerCertificate=false&useSSL=false");

			PreparedStatement p = conn.prepareStatement("SELECT count(*) as c FROM DWH.`Data`");
			ResultSet rs = p.executeQuery();

			rs.next();
			if(rs.getInt("c")<=0)
				caricaData(conn);
			else
				System.out.println("Dimensione Data gi� caricata");

			p = conn.prepareStatement("select count(*)  as c from DWH.meteo");
			rs = p.executeQuery();
			rs.next();
			if(rs.getInt("c")<=0)
				meteo(conn);
			else
				System.out.println("Dimensione Meteo gi� caricata");

			p = conn.prepareStatement("select count(*)  as c from DWH.orario");
			rs = p.executeQuery();
			rs.next();
			if(rs.getInt("c")<=0)
				caricaOrario(conn);
			else
				System.out.println("Dimensione Orario gi� caricata");

			conn.close();
		}catch(SQLException e){
			System.out.println(e.getMessage());
		}

	}

	private void caricaOrario(Connection conn) throws SQLException {

		String query = "LOAD DATA LOCAL INFILE 'tmpout/Ora/dimensione_orario.csv' "
				+ "INTO TABLE Orario FIELDS TERMINATED BY ';' "
				+ "ENCLOSED BY '\"' "
				+ "LINES TERMINATED BY '\n'	"
				+ "IGNORE 1 LINES (secondi,minuti,ore);";

		PreparedStatement p = conn.prepareStatement(query);
		p.execute();
	}

	private void meteo(Connection conn) throws SQLException {
		/*
		String query = "INSERT INTO Meteo (idMeteo, temperaturaMedia, temperaturaMinima, temperaturaMassima, precipitazioni, umidit�, ventoMassimo, raffica, fenomeni, insertTime, updateTime) "
				+ "SELECT null, TRIM(BOTH ' �C' FROM temperaturaMedia), TRIM(BOTH ' �C' FROM temperaturaMinima), TRIM(BOTH ' �C' FROM temperaturaMassima), "
				+ "precipitazioni, IF(umidit� = '-', ?, TRIM(BOTH '%' FROM umidit�)), IF(ventoMassimo='-', ?, TRIM(BOTH ' km/h' FROM ventoMassimo)),"
				+ "IF(raffica = '-', ?, TRIM(BOTH ' km/h' FROM raffica)), fenomeni, insertTime, updateTime "
				+ "FROM ODS.Meteo_ODS;";
		 */
		String query_per_file = "SELECT data, TRIM(BOTH ' �C' FROM temperaturaMedia) as tme, "
				+"TRIM(BOTH ' �C' FROM temperaturaMinima) as tmin, "
				+"TRIM(BOTH ' �C' FROM temperaturaMassima) as tmax, precipitazioni, "
				+"TRIM(BOTH '%' FROM umidit�) as um, "
				+"TRIM(BOTH ' km/h' FROM ventoMassimo) as vento, "
				+"TRIM(BOTH ' km/h' FROM raffica) as raffica, fenomeni, updateTime "
				+"FROM ODS.Meteo_ODS;";

		PreparedStatement p = conn.prepareStatement(query_per_file);
		ResultSet rs = p.executeQuery();

		scriviFile(rs, conn);
	}

	private void scriviFile(ResultSet rs, Connection conn) {

		try {
			FileWriter f = new FileWriter("tmpout/Meteo/dimensione_meteo.csv", true);

			while(rs.next()){
				f.append(rs.getString("data")+";"+rs.getString("tme")+";"+rs.getString("tmin")+";"+rs.getString("tmax")+";"+rs.getString("precipitazioni")+
						";"+rs.getString("um")+";"+rs.getString("vento")+";"+rs.getString("raffica")+";"+rs.getString("fenomeni")+";"
						+rs.getString("updateTime")+"\n");
			}
			f.close();

			caricaMeteo(conn);
		} catch (IOException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void caricaData(Connection conn) throws SQLException {

		String query = "LOAD DATA LOCAL INFILE 'tmpout/Data/dimensione_data.csv' "
				+ "INTO TABLE `Data` FIELDS TERMINATED BY ';' "
				+ "ENCLOSED BY '\"' "
				+ "LINES TERMINATED BY '\n' IGNORE 1 LINES"
				+ "	(`data`, `giorno`, `giornoSettimana`, `mese`, `bimestre`, `trimestre`, `quadrimestre`, `semestre`, `anno`);";

		PreparedStatement p = conn.prepareStatement(query);
		p.execute();

	}

	private void caricaMeteo(Connection conn) throws SQLException {

		String query = "LOAD DATA LOCAL INFILE 'tmpout/Meteo/dimensione_meteo.csv' "
				+ "INTO TABLE `Meteo` FIELDS TERMINATED BY ';' "
				+ "ENCLOSED BY '\"' "
				+ "	(data, temperaturaMedia, temperaturaMinima, temperaturaMassima, precipitazioni, umidita, ventoMassimo, raffica, fenomeni, updateTime);";

		PreparedStatement p = conn.prepareStatement(query);
		p.execute();

	}

}

