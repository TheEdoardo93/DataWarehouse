package processoETL_ODS_DWH;

import java.io.FileWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class UpdatePedoniPersoneInAuto {

	/*
	public static void main(String[] args) {
		startUpdate();
	}
	*/

	public void startUpdate() {

		ArrayList<ArrayList<String>> personeInAuto = caricaPersoneInAutoODS();

		ArrayList<ArrayList<String>> pedoni =  caricaPedoniODS();

		ArrayList<ArrayList<String>> veicoli = caricaVeicoliDWH();

		personeInAuto = trasformazioniPersoneInAutoDWH(personeInAuto, veicoli);

		int i = scriviSuCSVPers(personeInAuto);
		scriviSuCSVPed(pedoni, i);

	}

	private boolean scritturaSuDatabase(String file) {
		Connection conn = null;
		PreparedStatement statement = null;
		try {
			String query_load = "LOAD DATA LOCAL INFILE '" + file + "'" +
					" INTO TABLE DWH.PersoneCoinvolte FIELDS TERMINATED BY ';' ENCLOSED BY '\"'" + 
					" LINES TERMINATED BY '\n' IGNORE 1 LINES (idPersona,tipoPersona,sesso,annoNascita,tipoLesione,deceduto,decedutoDopo,progressivo,"
					+ "cinturaCascoUtilizzato,airbag,conducente,Veicoli_idVeicolo);";
			conn = DriverManager.getConnection("jdbc:mysql://localhost/dwh?user=root&password=password&verifyServerCertificate=false&useSSL=true");
			statement = conn.prepareStatement(query_load);
			statement.execute();
			conn.close();
			return true;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return false;
	}

	private int scriviSuCSVPers(ArrayList<ArrayList<String>> persone) {

		String csv_header = "idPersona;tipoPersona;sesso;annoNascita;tipoLesione;deceduto;decedutoDopo;progressivo;cinturaCascoUtilizzato;"
				+ "airbag;conducente;Veicoli_idVeicolo";
		FileWriter writer = null;

		try {
			writer = new FileWriter("tmpout/pedoniPersone/dwhPersoneInAuto.csv", true);
			writer.append(csv_header.toString());
			writer.append("\n");
			int i =0;
			
			for (i = 0; i < persone.size(); i++) {
				writer.append((i+1)+";");
				for (int j = 0; j < persone.get(i).size()-2; j++) {
					writer.append(persone.get(i).get(j)+";");
				}
				writer.append(persone.get(i).get(persone.get(i).size()-2)+"\n");
			}
			
			writer.close();
			boolean risultato = scritturaSuDatabase("tmpout/pedoniPersone/dwhPersoneInAuto.csv");
			if (risultato == false) {
				System.out.println("Attenzione: problemi nella scrittura su database della dimensione 'PersoneCoinvolte'!");
			}
			return i;
		} catch (Exception e) {
			System.out.println("\t\tErrore nella creazione del file di output in formato .csv!");
			e.printStackTrace();
		}
		return 0;
	}

	private void scriviSuCSVPed(ArrayList<ArrayList<String>> pedoni, int cont) {

		String csv_header = "idPersona;tipoPersona;sesso;annoNascita;tipoLesione;deceduto;decedutoDopo";
		FileWriter writer = null;

		try {
			writer = new FileWriter("tmpout/pedoniPersone/dwhPedoni.csv", true);
			writer.append(csv_header.toString());
			writer.append("\n");

			
			for (int i = 0; i < pedoni.size(); i++) {
				writer.append((i+cont +1) + ";" );
				for (int j = 0; j < pedoni.get(i).size()-2; j++) {
					writer.append(pedoni.get(i).get(j)+";");
				}
				writer.append(pedoni.get(i).get(pedoni.get(i).size()-2)+"\n");
			}

			writer.close();
			boolean risultato = scritturaSuDatabase("tmpout/pedoniPersone/dwhPedoni.csv");
			if (risultato == false) {
				System.out.println("Attenzione: problemi nella scrittura su database della dimensione 'PersoneCoinvolte'!");
			}
		} catch (Exception e) {
			System.out.println("\t\tErrore nella creazione del file di output in formato .csv!");
			e.printStackTrace();
		}
	}

	private ArrayList<ArrayList<String>> trasformazioniPersoneInAutoDWH(ArrayList<ArrayList<String>> personeInAuto, ArrayList<ArrayList<String>> veicoli) {

		System.out.println("\t\tInizio trasformazioni");
		//Aggiunta della chiave esterna del veicolo corrispondente al dwh e aggiunta del booleano conducente

		for(ArrayList<String> pia : personeInAuto){
			if(pia.get(0).equalsIgnoreCase("passeggero"))
				pia.add(9,"0");
			else
				pia.add(9,"1");
		}

		System.out.println("\t\tAggiunta colonna conducente");

		for(ArrayList<String> pia : personeInAuto){
			for(ArrayList<String> vei : veicoli){
				if(pia.get(10).equals(vei.get(1))){
					pia.remove(10);
					pia.add(10, vei.get(0));;
					break;
				}
			}

		}

		System.out.println("\t\tChiavi esterne con veicolo sistemate");

		return personeInAuto;
	}

	@SuppressWarnings("unchecked")
	private ArrayList<ArrayList<String>> caricaPersoneInAutoODS() {

		ArrayList<ArrayList<String>> personeInAuto = new ArrayList<ArrayList<String>>();
		ArrayList<String> tmp = new ArrayList<String>();

		Connection conn = null;


		try {
			conn = DriverManager.getConnection("jdbc:mysql://localhost/ODS?user=root&password=password&verifyServerCertificate=false&useSSL=true");

			String query = "SELECT progressivo, tipoPersona, annoNascita, sesso,"
					+ " tipoLesione, deceduto, cinturaCascoUtilizzato, airbag,decedutoDopo, idProtocollo, idVeicolo"
					+ " FROM ODS.PersoneInAuto_ODS"
					+ " where ODS.PersoneInAuto_ODS.updateTime > (select coalesce(max(date(DWH.PersoneCoinvolte.updateTime)),0) from DWH.PersoneCoinvolte)";

			PreparedStatement prst = conn.prepareStatement(query);

			ResultSet rs = prst.executeQuery();

			while (rs.next()) {
				tmp.add(rs.getString("tipoPersona"));
				tmp.add(rs.getString("sesso"));
				tmp.add(rs.getString("annoNascita"));
				tmp.add(rs.getString("tipoLesione"));
				tmp.add(rs.getString("deceduto"));
				tmp.add(rs.getString("decedutoDopo"));
				tmp.add(rs.getString("progressivo"));
				tmp.add(rs.getString("cinturaCascoUtilizzato"));
				tmp.add(rs.getString("airbag"));
				tmp.add(rs.getString("idVeicolo"));
				tmp.add(rs.getString("idProtocollo"));

				personeInAuto.add((ArrayList<String>) tmp.clone());

				tmp.clear();
			}

			conn.close();
			
			System.out.println("Dimensione PersoneInAuto: " + personeInAuto.size());
			return personeInAuto;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;


	}

	@SuppressWarnings("unchecked")
	private ArrayList<ArrayList<String>> caricaPedoniODS() {
		ArrayList<ArrayList<String>> pedoni = new ArrayList<ArrayList<String>>();
		ArrayList<String> tmp = new ArrayList<String>();

		Connection conn = null;


		try {
			conn = DriverManager.getConnection("jdbc:mysql://localhost/ODS?user=root&password=password&verifyServerCertificate=false&useSSL=true");

			String query = "SELECT tipoPersona, annoNascita, sesso,"
					+ " tipoLesione, deceduto,decedutoDopo, idProtocollo"
					+ " FROM ODS.Pedoni_ODS"
					+ " where ODS.Pedoni_ODS.updateTime > (select coalesce(max(date(DWH.PersoneCoinvolte.updateTime)),0) from DWH.PersoneCoinvolte)";

			PreparedStatement prst = conn.prepareStatement(query);

			ResultSet rs = prst.executeQuery();

			while(rs.next()){
				tmp.add(rs.getString("tipoPersona"));
				tmp.add(rs.getString("sesso"));
				tmp.add(rs.getString("annoNascita"));
				tmp.add(rs.getString("tipoLesione"));
				tmp.add(rs.getString("deceduto"));
				tmp.add(rs.getString("decedutoDopo"));
				tmp.add(rs.getString("idProtocollo"));

				pedoni.add((ArrayList<String>) tmp.clone());

				tmp.clear();
			}

			conn.close();
			
			System.out.println("Dimensione PersoneInAuto: " + pedoni.size());
			return pedoni;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	@SuppressWarnings("unchecked")
	private ArrayList<ArrayList<String>> caricaVeicoliDWH() {
		ArrayList<ArrayList<String>> veicoli = new ArrayList<ArrayList<String>>();
		ArrayList<String> tmp = new ArrayList<String>();

		Connection conn = null;


		try {
			conn = DriverManager.getConnection("jdbc:mysql://localhost/DWH?user=root&password=password&verifyServerCertificate=false&useSSL=true");

			String query = "SELECT idVeicolo, veicolo FROM DWH.Veicoli";

			PreparedStatement prst = conn.prepareStatement(query);

			ResultSet rs = prst.executeQuery();

			while(rs.next()){
				tmp.add(rs.getString("idVeicolo"));
				tmp.add(rs.getString("veicolo"));

				veicoli.add((ArrayList<String>) tmp.clone());

				tmp.clear();
			}

			conn.close();
			System.out.println("Dimensione Veicoli: " + veicoli.size());
			return veicoli;

		}catch(SQLException e){
			System.out.println(e.getMessage());
		}
		return null;
	}


}
