package factTable;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class createFact {

	public static void main(String[] args) {

		System.out.println("---Inizio del processo di creazione del fatto 'Incidenti'---");

		boolean risultato = startCreateFact();
		if (risultato == true) {
			System.out.println("Il processo di creazione del fatto � andato a buon fine");
		} else {
			if (risultato == false) {
				System.out.println("Attenzione: il processo di creazione del fatto non � andato a buon fine");
			}
		}

		System.out.println("---Fine del processo di creazione del fatto 'Incidenti'---");
	}

	private static boolean startCreateFact() {
		String [] from = new String [7];

		from[0] = " FROM DWH.incidenti_2010";
		from[1] = " FROM DWH.incidenti_2011";
		from[2] = " FROM DWH.incidenti_2012";
		from[3] = " FROM DWH.incidenti_2013";
		from[4] = " FROM DWH.incidenti_2014";
		from[5] = " FROM DWH.incidenti_2015";
		from[6] = " FROM DWH.incidenti_2016";

		ResultSet rs = null;
		boolean risultato = true;

		for(int i = 0; i < from.length; i++){

			rs = caricaVista(from[i]);

			String [] condizioni_incidente = new String[4];
			String [] informazioni_incidente = new String[2];
			String [] misure = new String [5];

			String csv_header = "idIncidente;numeroFeriti;numeroRiservata;numeroMorti;numeroIllesi;confermato;"
					+ "idData;idMeteo;idOrario;idCondizioniIncidenti;idInformazioniIncidenti;idPosizioneGeografica;idHelper_Incidenti_Strade;"
					+ "idHelper_Incidenti_PersoneCoinvolte;idHelper_Incidenti_Veicoli";
			FileWriter writer = null;

			try {
				writer = new FileWriter("tmpout/Fatto/"+from[i].substring(10)+".csv", true);
				writer.append(csv_header.toString()+"\n");
			} catch(IOException e) {
				System.out.println(e.getMessage());
			}

			try {
				Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/DWH?user=root&password=password&verifyServerCertificate=false&useSSL=true");
				while (rs.next()) {

					condizioni_incidente[0]= rs.getString("illuminazione");
					condizioni_incidente[1]= rs.getString("traffico");
					condizioni_incidente[2]= rs.getString("visibilita");
					condizioni_incidente[3]= rs.getString("condizioneAtmosferica");

					int condizioniIncidenti = getCondizioniIncidenteKey(condizioni_incidente);

					informazioni_incidente[0]= rs.getString("naturaIncidente");
					informazioni_incidente[1]= rs.getString("gruppo");

					int informazioniIncidenti = getInformazioniIncidenteKey(informazioni_incidente);

					String idIncidente = rs.getString("idIncidente");

					int idLuogo = rs.getInt("idLuogo");

					int data = getIdData(rs.getString("dataIncidente"));

					int ora = gatIdOra(rs.getString("oraIncidente"));

					int meteo = getIdMeteo(rs.getString("dataIncidente"));

					int helper_strade = getIdStrade(idLuogo);

					int helper_veicoli = getIdVeicoli(idIncidente);

					int helper_persone = getIdPersone(idIncidente);

					misure [0] = rs.getString("numeroFeriti"); 
					misure [1] = rs.getString("numeroRiservata");
					misure [2] = rs.getString("numeroMorti");
					misure [3] = rs.getString("numeroIllesi");
					misure [4] =rs.getString("confermato");

					risultato = risultato && scriviSingoloFattoSuCSV(idIncidente, misure, condizioniIncidenti, informazioniIncidenti, idLuogo, data, meteo, ora,
							helper_strade, helper_veicoli, helper_persone, writer);
				}
				writer.close();
				conn.close();
			} catch (SQLException | IOException e) {
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
		return risultato;
	}


	private static ResultSet caricaVista(String from) {

		/*Dalla vista vengono caricati i seguenti campi:
		 * 
		 * idProtocollo, gruppo, dataIncidente,oraIncidenti, naturaIncidente, condizioneAtmosferica, traffic, visibilita, illuminazione, 
		 * numeroferiti, ilesi, morti, riservata, confermato
		 * 
		 */

		Connection conn = null;

		try {
			conn = DriverManager.getConnection("jdbc:mysql://localhost/DWH?user=root&password=password&verifyServerCertificate=false&useSSL=true");
			String query = "SELECT *" + from;

			PreparedStatement p = conn.prepareStatement(query);
			ResultSet rs = p.executeQuery();
			//conn.close();
			return rs;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

	private static boolean scriviSingoloFattoSuCSV(String idIncidente, String[] misure, int condizioniIncidenti, int informazioniIncidenti, int idLuogo,
			int data, int meteo, int ora, int helper_strade, int helper_veicoli, int helper_persone, FileWriter writer) {

		try {
			writer.append(idIncidente+";"+misure[0]+";"+misure[1]+";"+misure[2]+";"+misure[3]+";"+misure[4]+";"+data+";"+meteo+";"+ora+";"+
					condizioniIncidenti+";"+informazioniIncidenti+";"+idLuogo+";"+helper_strade+";"+helper_persone+";"+helper_veicoli + "\n");

			System.out.println(idIncidente+";"+misure[0]+";"+misure[1]+";"+misure[2]+";"+misure[3]+";"+misure[4]+";"+data+";"+meteo+";"+ora+";"+
					condizioniIncidenti+";"+informazioniIncidenti+";"+idLuogo+";"+helper_strade+";"+helper_persone+";"+helper_veicoli + "\n");

			/*
			boolean risultato = scritturaFattoSuDatabase();
			if (risultato == false) {
				System.out.println("Attenzione: problemi nella scrittura su database del fatto 'Incidenti'!");
			}*/
		} catch (Exception e) {
			System.out.println("\t\tErrore nella creazione del file di output in formato .csv!");
			e.printStackTrace();
		}
		return true;
	}

	private static boolean controlloPrimaVolta() {

		String fileCSV = "/tmpout/Fatto/FattoDWH.csv";
		BufferedReader br = null;
		String linea = "";
		String csvSplitBy = ";";
		boolean primaVolta = false;

		try {
			br = new BufferedReader(new FileReader(fileCSV));
			if ((linea = br.readLine()) != null) {
				String[] headerFileCSV = linea.split(csvSplitBy);

				if (headerFileCSV[0].equalsIgnoreCase("idIncidente")) {
					primaVolta = true;
				} else {
					primaVolta = false;
				}
			}
			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return primaVolta;
	}

	private static boolean scritturaFattoSuDatabase() {

		Connection conn = null;
		PreparedStatement statement = null;
		String file = "'tmpout/Fatto/fattoDWH.csv'";

		try {
			String query_load = "LOAD DATA LOCAL INFILE " + file +
					" INTO TABLE DWH.Incidenti FIELDS TERMINATED BY ';' ENCLOSED BY '\"'" + 
					" LINES TERMINATED BY '\n' IGNORE 1 LINES (incidente,numeroFeriti,numeroRiservata,numeroMorti,numeroIllesi,confermato,"
					+ "idData,idMeteo,idOrario,idCondizioniIncidenti,idInformazioniIncidenti,idPosizioneGeografica,idHelper_Incidenti_Strade,"
					+ "idHelper_Incidenti_PersoneCoinvolte,idHelper_Incidenti_Veicoli);";

			conn = DriverManager.getConnection("jdbc:mysql://localhost/dwh?user=root&password=password&verifyServerCertificate=false&useSSL=true");
			statement = conn.prepareStatement(query_load);
			statement.execute();
			conn.close();
			return true;
		} catch (SQLException e) {
			e.getSQLState();
			e.getErrorCode();
			System.out.println(e.getMessage());
		}
		return false;
	}

	//JUNK INFROMAZIONI INCIDENTI
	private static int getInformazioniIncidenteKey(String[] informazioni_incidente) {
		String query = "SELECT idInformazioniIncidenti FROM DWH.InformazioniIncidenti "
				+ "WHERE naturaIncidente = ? AND gruppo = ?";
		Connection conn = null;

		try {
			conn = DriverManager.getConnection("jdbc:mysql://localhost/DWH?user=root&password=password&verifyServerCertificate=false&useSSL=true");
			PreparedStatement p = conn.prepareStatement(query);
			p.setString(1, informazioni_incidente[0]);
			p.setString(2, informazioni_incidente[1]);

			ResultSet rs = p.executeQuery();
			rs.next();
			int key = rs.getInt("idInformazioniIncidenti");
			conn.close();
			return key;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		return -1;
	}

	//JUNK CONDIZIONI INCIDENTI
	private static int getCondizioniIncidenteKey(String[] condizioni_incidente) {

		String query = "SELECT idCondizioniIncidenti FROM DWH.CondizioniIncidenti "
				+ "WHERE illuminazione = ? AND traffico = ? AND visibilita = ? AND condizioneAtmosferica = ?";

		Connection conn = null;
		try {
			conn = DriverManager.getConnection("jdbc:mysql://localhost/DWH?user=root&password=password&verifyServerCertificate=false&useSSL=true");

			PreparedStatement p = conn.prepareStatement(query);

			p.setString(1, condizioni_incidente[0]);
			p.setString(2, condizioni_incidente[1]);
			p.setString(3, condizioni_incidente[2]);
			p.setString(4, condizioni_incidente[3]);

			ResultSet rs = p.executeQuery();
			rs.next();
			int key = rs.getInt("idCondizioniIncidenti"); 
			conn.close();
			return key;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		return -1;
	}



	//PERSONE
	private static int getIdPersone(String idIncidente) {

		String pers = "SELECT idPersonaInAuto FROM ods.Personeinauto_ods WHERE ODS.Personeinauto_ods.idProtocollo=?";

		String ped = "SELECT idPedone FROM ods.pedoni_ods WHERE ODS.pedoni_ods.idProtocollo=?";

		String query_count = "SELECT count(*) as c from ods.PersoneInAuto_ods";

		PreparedStatement p = null;
		Connection conn = null;

		try {
			conn = DriverManager.getConnection("jdbc:mysql://localhost/DWH?user=root&password=password&verifyServerCertificate=false&useSSL=true");
			p = conn.prepareStatement(query_count);
			ResultSet  rs = p.executeQuery();
			rs.next();
			int count_persone = rs.getInt("c");

			p = conn.prepareStatement(pers);
			p.setString(1, idIncidente);

			rs = p.executeQuery();
			ArrayList<Integer> persone = new ArrayList<Integer>();

			while (rs.next()) {
				persone.add(rs.getInt("idPersonaInAuto"));
			}

			p = conn.prepareStatement(ped);
			p.setString(1, idIncidente);

			rs = p.executeQuery();

			while (rs.next()) {
				persone.add(rs.getInt("idPedone") + count_persone + 1);
			}

			if (persone != null) {
				ArrayList<Integer> persone_dwh = getDWHPersone(persone);
				if (persone_dwh != null) {
					conn.close();
					return getIdHelperPersone(persone_dwh);
				}
				else {
					conn.close();
					return -1;
				}
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		return -1;
	}

	private static ArrayList<Integer> getDWHPersone(ArrayList<Integer> persone) {

		if (persone.size() > 0) {
			Connection conn = null;
			PreparedStatement p = null;
			ResultSet rs = null;
			ArrayList<Integer> persone_dwh = new ArrayList<Integer>();
			String query = "SELECT idPersona FROM DWH.personecoinvolte WHERE PersoneCoinvolte.idPersona=?";

			try{
				conn = DriverManager.getConnection("jdbc:mysql://localhost/DWH?user=root&password=password&verifyServerCertificate=false&useSSL=true");

				for (int i =0; i < persone.size(); i++){
					p = conn.prepareStatement(query);
					p.setInt(1, persone.get(i));
					rs = p.executeQuery();
					while (rs.next()) {
						persone_dwh.add(rs.getInt("idPersona"));
					}
				}
				conn.close();
				return persone_dwh;
			} catch(SQLException e) {
				System.out.println(e.getMessage());
			}
		}
		return null;
	}

	private static int getIdHelperPersone(ArrayList<Integer> persone_dwh) {

		String query1 = "SELECT DISTINCT(a.idHelper_Incidenti_PersoneCoinvolte) FROM DWH.Helper_Incidenti_PersoneCoinvolte as a,"
				+ " DWH.Helper_Incidenti_PersoneCoinvolte as b WHERE a.idHelper_Incidenti_PersoneCoinvolte = b.idHelper_Incidenti_PersoneCoinvolte"
				+ " AND a.PersoneCoinvolte_idPersona = ?"
				+ " GROUP BY a.idHelper_Incidenti_PersoneCoinvolte"
				+ " HAVING COUNT(a.idHelper_Incidenti_PersoneCoinvolte) = 1;";

		String query2= "SELECT a0.idHelper_Incidenti_PersoneCoinvolte FROM ";
		String from = "";

		ResultSet rs = null;
		Connection conn = null;

		try {
			conn = DriverManager.getConnection("jdbc:mysql://localhost/DWH?user=root&password=password&verifyServerCertificate=false&useSSL=true");
			PreparedStatement p = null;
			if (persone_dwh.size() == 1) {
				p = conn.prepareStatement(query1);
				p.setInt(1, persone_dwh.get(0));
				rs = p.executeQuery();
			} 
			else {
				int i=0;
				for(; i<persone_dwh.size()-1;i++)
					from = from + "(SELECT idHelper_Incidenti_PersoneCoinvolte FROM DWH.Helper_Incidenti_PersoneCoinvolte WHERE PersoneCoinvolte_idPersona=?) as a"+i+" join ";

				from = from + "(SELECT idHelper_Incidenti_PersoneCoinvolte FROM DWH.Helper_Incidenti_PersoneCoinvolte WHERE PersoneCoinvolte_idPersona=?) as a"+i;
				p = conn.prepareStatement(query2 + from);
				for(i =0; i<persone_dwh.size();i++)
					p.setInt(i+1, persone_dwh.get(i));
				rs = p.executeQuery();
			}
			
			if(rs.next()){
				int key = rs.getInt("idHelper_Incidenti_PersoneCoinvolte");
				conn.close();
				return key;
			}
			else
				return -1;
		} catch (SQLException e) {
			System.out.println("ERRORE NELLE HELPER PERSONE " + e.getMessage());
			e.printStackTrace();
		}
		return -1;
	}


	//VEICOLI
	private static int getIdVeicoli(String idIncidente) {

		String query_strada_ods ="SELECT idVeicolo FROM ods.Veicoli_ODS WHERE ODS.veicoli_ods.idProtocollo=?;";

		Connection conn = null;
		PreparedStatement p = null;

		try {
			conn = DriverManager.getConnection("jdbc:mysql://localhost/DWH?user=root&password=password&verifyServerCertificate=false&useSSL=true");
			p = conn.prepareStatement(query_strada_ods);
			p.setString(1, idIncidente);

			ResultSet rs = p.executeQuery();
			ArrayList<Integer> veicoli = new ArrayList<Integer>();

			while (rs.next()) {
				veicoli.add(rs.getInt("idVeicolo"));
			}
			ArrayList<Integer> veicoli_dwh = getDWHVeicoli(veicoli);

			conn.close();
			return getIdHelperVeicoli(veicoli_dwh);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		return 0;
	}

	private static ArrayList<Integer> getDWHVeicoli(ArrayList<Integer> veicoli) {
		Connection conn = null;
		PreparedStatement p = null;
		ResultSet rs = null;

		ArrayList <Integer> veicoli_dwh = new ArrayList <Integer>();
		String query = "SELECT idVeicolo from DWH.veicoli where veicolo = ?";

		try{
			conn = DriverManager.getConnection("jdbc:mysql://localhost/DWH?user=root&password=password&verifyServerCertificate=false&useSSL=true");

			for (int i = 0; i < veicoli.size(); i++){
				p = conn.prepareStatement(query);
				p.setInt(1, veicoli.get(i));
				rs = p.executeQuery();
				while (rs.next()) {
					veicoli_dwh.add(rs.getInt("idVeicolo"));
				}
			}
			conn.close();
			return veicoli_dwh;
		} catch(SQLException e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

	private static int getIdHelperVeicoli(ArrayList<Integer> veicoli) {
		
		if(veicoli.size()==0)
			return -1;

		String query1 = "SELECT DISTINCT(a.idHelper_Incidenti_Veicoli) FROM DWH.Helper_Incidenti_Veicoli as a, DWH.Helper_Incidenti_Veicoli as b" +
				" WHERE a.idHelper_Incidenti_Veicoli = b.idHelper_Incidenti_Veicoli AND a.Veicoli_idVeicolo = ?" +
				" GROUP BY a.idHelper_Incidenti_Veicoli HAVING COUNT(a.idHelper_Incidenti_Veicoli) = 1;";

		String query2= "SELECT v0.idHelper_Incidenti_Veicoli FROM ";
		String from = "";

		Connection conn = null;
		PreparedStatement p = null;

		try {
			conn = DriverManager.getConnection("jdbc:mysql://localhost/DWH?user=root&password=password&verifyServerCertificate=false&useSSL=true");
			ResultSet rs;
			if (veicoli.size() == 1) {
				p = conn.prepareStatement(query1);
				p.setInt(1, veicoli.get(0));
				rs = p.executeQuery();
			} 
			else{
				int i;
				for (i = 0; i < veicoli.size() - 1; i++){
					from = from + "(SELECT idHelper_Incidenti_Veicoli FROM DWH.Helper_Incidenti_Veicoli WHERE Veicoli_idVeicolo=?) as v"+i+" join ";
				}
				from = from + "(SELECT idHelper_Incidenti_Veicoli FROM DWH.Helper_Incidenti_Veicoli WHERE Veicoli_idVeicolo=?) as v"+i;

				p = conn.prepareStatement(query2 + from);

				for(i = 0; i < veicoli.size() ;i++)
					p.setInt(i+1, veicoli.get(i));
				rs = p.executeQuery();
			}
			rs.next();
			int key = rs.getInt("idHelper_Incidenti_Veicoli");
			conn.close();
			return key;
		} catch (SQLException e) {
			System.out.println("Errore nella HELPER VEICOLI. \n Veicoli cercati: " + veicoli);
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		return -1;
	}



	//STRADE
	private static int getIdStrade(int idLuogo) {

		String query_strada_ods =" SELECT strada FROM ODS.Strade_ODS, ods.luoghi_strade_ods "
				+ "WHERE ODS.luoghi_strade_ods.idLuogo=? AND ODS.luoghi_strade_ods.idStrada = ods.strade_ods.idStrada";
		Connection conn = null;
		PreparedStatement p = null;

		try {
			conn = DriverManager.getConnection("jdbc:mysql://localhost/DWH?user=root&password=password&verifyServerCertificate=false&useSSL=true");
			p = conn.prepareStatement(query_strada_ods);
			p.setInt(1, idLuogo);

			ResultSet rs = p.executeQuery();
			ArrayList<String> strade = new ArrayList<String>();

			while (rs.next()) {
				strade.add(rs.getString("strada"));
			}
			ArrayList<Integer> strade_dwh = getDWHStrade(strade);

			conn.close();
			return getIdHelperStrade(strade_dwh);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		return 0;
	}

	private static ArrayList<Integer> getDWHStrade(ArrayList<String> strade) {

		ArrayList <Integer> strade_dwh = new ArrayList <Integer>();

		String query = "SELECT idStrada FROM dwh.strade where strada=?;";
		Connection conn = null;
		PreparedStatement p = null;
		ResultSet rs = null;

		try {
			conn = DriverManager.getConnection("jdbc:mysql://localhost/DWH?user=root&password=password&verifyServerCertificate=false&useSSL=true");

			for (int i = 0; i < strade.size(); i++) {
				p = conn.prepareStatement(query);
				p.setString(1, strade.get(i));
				rs = p.executeQuery();
				while(rs.next()) {
					strade_dwh.add(rs.getInt("idStrada"));
				}
			}
			conn.close();
			return strade_dwh;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

	private static int getIdHelperStrade(ArrayList<Integer> strade_dwh) {

		Connection conn = null;

		String query2 = "SELECT a.idHelper_Incidenti_Strade FROM DWH.Helper_Incidenti_Strade as a," +
				" DWH.Helper_Incidenti_Strade as b WHERE a.idHelper_Incidenti_Strade = b.idHelper_Incidenti_Strade AND" +
				" a.Strade_idStrada = ?  and b.Strade_idStrada = ?;";

		String query1 = "SELECT DISTINCT(a.idHelper_Incidenti_Strade) FROM DWH.Helper_Incidenti_Strade as a," +
				" DWH.Helper_Incidenti_Strade as b WHERE a.idHelper_Incidenti_Strade = b.idHelper_Incidenti_Strade AND" +
				" a.Strade_idStrada = ? GROUP BY a.idHelper_Incidenti_Strade HAVING COUNT(a.idHelper_Incidenti_Strade) = 1;";

		try {
			conn = DriverManager.getConnection("jdbc:mysql://localhost/DWH?user=root&password=password&verifyServerCertificate=false&useSSL=true");
			PreparedStatement p = null;
			if (strade_dwh.size() == 1) {
				p = conn.prepareStatement(query1);
				p.setInt(1, strade_dwh.get(0));
				ResultSet rs = p.executeQuery();
				rs.next();
				int key = rs.getInt("idHelper_Incidenti_Strade");
				conn.close();
				return key;
			} 
			else {
				if (strade_dwh.size() == 2) {
					p = conn.prepareStatement(query2);
					p.setInt(1, strade_dwh.get(0));
					p.setInt(2, strade_dwh.get(1));
					ResultSet rs = p.executeQuery();
					rs.next();
					int key = rs.getInt("idHelper_Incidenti_Strade");
					conn.close();
					return key;
				}
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		return -1;
	}

	//METEO
	private static int getIdMeteo(String data) {

		String query = "SELECT idMeteo FROM DWH.Meteo WHERE data = ?";
		Connection conn = null;

		try {
			conn = DriverManager.getConnection("jdbc:mysql://localhost/DWH?user=root&password=password&verifyServerCertificate=false&useSSL=true");
			PreparedStatement p = conn.prepareStatement(query);
			p.setString(1, data);
			ResultSet rs = p.executeQuery();
			rs.next();
			int key = rs.getInt("idMeteo");
			conn.close();
			return key;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		return -1;
	}

	//ORA
	private static int gatIdOra(String ora) {

		String query = "SELECT idOrario FROM DWH.Orario WHERE ore = hour(?) AND "
				+ "minuti = minute(?) AND secondi = second(?)";
		Connection conn = null;

		try {
			conn = DriverManager.getConnection("jdbc:mysql://localhost/DWH?user=root&password=password&verifyServerCertificate=false&useSSL=true");
			PreparedStatement p = conn.prepareStatement(query);

			p.setString(1, ora);
			p.setString(2, ora);
			p.setString(3, ora);

			ResultSet rs = p.executeQuery();
			rs.next();
			int key = rs.getInt("idOrario");
			conn.close();
			return key;
		} catch (SQLException e) {
			System.out.println("Eccezione orario. Ora cercata: " + ora);
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		return -1;
	}

	//DATA
	private static int getIdData(String data) {

		String query = "SELECT idData FROM DWH.Data WHERE data = ?";
		Connection conn = null;

		try {
			conn = DriverManager.getConnection("jdbc:mysql://localhost/DWH?user=root&password=password&verifyServerCertificate=false&useSSL=true");
			PreparedStatement p = conn.prepareStatement(query);
			p.setString(1, data);
			ResultSet rs = p.executeQuery();
			rs.next();
			int key = rs.getInt("idData");
			conn.close();
			return key;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		return -1;
	}
}
